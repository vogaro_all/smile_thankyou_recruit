import Swiper from 'swiper/dist/js/swiper'
import 'intersection-observer'

export default class {
  constructor() {
    this.target = document.getElementById('js-footer-swiper')
    this.slider = null
    this.mql = window.matchMedia('only screen and (max-width: 767px)')
  }

  init() {
    if (!this.target) return
    this.createSlider()
    this.handleEvents()
  }

  handleEvents() {
    window.addEventListener('resize', this.onResize.bind(this))
  }

  createSlider() {
    this.slider = new Swiper(this.target, {
      init: false,
      loop: true,
      slidesPerView: 1,
      spaceBetween: 0,
      observer: true,
      observeParents: true,
      effect: 'fade',
      onSlideChangeEnd: s => {
        s.fixLoop()
      },
      autoplay: {
        delay: 4000, // ４秒後に次のスライドへ
        disableOnInteraction: false // ユーザー側で操作してもスライドを止めない
      },
      simulateTouch: false,
      speed: 2000,
      breakpoints: {
        767: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      }
    })
    this.slider.update()
    this.slider.init()
  }

  onResize() {
    this.slider.update()
  }
}

import 'core-js/features/promise'

import _ from 'lodash'

export default class Loading {
  constructor(paths = [], callback) {
    this.loading = document.getElementById('js-loading')
    this.loadingProgress = document.getElementById('js-loading-progress')
    this.promises = []
    this.paths = paths
    this.totalImages = paths.length
    this.currentLoadedImages = 0
    this.progressNum = 0
    this.progressOfImagesLoading = 0
    this.callback = callback
  }

  init() {
    this.onLoading()
  }

  onLoading() {
    _.forEach(this.paths, path => {
      const promise = new Promise((resolve, reject) => {
        const img = new Image()
        const loaded = () => {
          this.currentLoadedImages++
          this.progressOfImagesLoading =
            this.currentLoadedImages / this.totalImages
          this.progressNum = Math.floor(this.progressOfImagesLoading * 100)
          this.loadingProgress.style.width = `${this.progressNum}%`

          resolve()
        }

        img.onload = loaded
        img.onerror = loaded
        img.src = path
      })

      this.promises.push(promise)
    })

    Promise.all(this.promises).then(() => {
      window.addEventListener('load', () => {
        this.onLoaded()
      })
    })
  }

  onLoaded() {
    document.documentElement.setAttribute('loading-state', 'done')
    if (this.callback) this.callback()
  }
}

export default class {
  constructor(targetSelector) {
    this.tabRoot = document.querySelector(targetSelector)
    this.triggers = [...this.tabRoot.querySelectorAll('[data-tab-target]')]
    this.contents = this.triggers.map(trigger =>
      document.getElementById(trigger.dataset.tabTarget)
    )
    this.wrapper = this.tabRoot.querySelector('.js-tab-wrapper')

    this.activeClass = 'is-active'
    this.animationClass = 'is-animation'
  }

  init() {
    this.handleEvents()
  }

  handleEvents() {
    this.triggers.forEach(trigger => {
      trigger.addEventListener('click', this.onClick.bind(this))
    })
  }

  onClick(event) {
    const trigger = event.currentTarget
    const target = document.getElementById(trigger.dataset.tabTarget)

    this.switchTab(target)
  }

  switchTab(target) {
    const isAnimation = this.tabRoot.classList.contains(this.animationClass)
    const closeContent = this.contents.filter(target =>
      target.classList.contains(this.activeClass)
    )

    if (isAnimation || target.classList.contains(this.activeClass)) return

    this.tabRoot.classList.add(this.animationClass)

    // active triggerの切り替え
    const targetId = target.getAttribute('id')
    this.triggers.forEach(trigger => {
      trigger.classList.remove(this.activeClass)

      if (trigger.dataset.tabTarget === targetId) {
        trigger.classList.add(this.activeClass)
      }
    })

    const callback = () => {
      this.tabRoot.classList.remove(this.animationClass)

      this.wrapper.style.height = ''

      target.classList.remove('is-ready')
      target.removeEventListener('transitionend', callback)

      closeContent.forEach(target => {
        target.classList.remove(this.activeClass)
      })
    }

    this.wrapper.style.height = this.wrapper.clientHeight + 'px'

    target.classList.add('is-ready')
    target.style.opacity = 0
    target.classList.add(this.activeClass)

    // reflow
    target.offsetHeight // eslint-disable-line
    this.wrapper.offsetHeight // eslint-disable-line

    this.wrapper.style.height = target.clientHeight + 'px'
    target.style.opacity = 1
    setTimeout(callback, 500)
  }

  checkCurrentTab(activeBtn) {
    const trigger = activeBtn
    const target = document.getElementById(trigger.dataset.tabTarget)

    this.switchTab(target)
  }
}

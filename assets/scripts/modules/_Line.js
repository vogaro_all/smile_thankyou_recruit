import _ from 'lodash'

export default class {
  constructor() {
    this.targets = [...document.querySelectorAll('.js-line')]
    this.circles = [...document.querySelectorAll('.js-circle')]
  }

  init() {
    this.handleEvents()
  }

  handleEvents() {
    window.addEventListener('scroll', _.throttle(this.onScroll.bind(this), 100))
  }

  onScroll() {
    this.updateLine()
    this.checkIntersecting()
  }

  updateLine() {
    this.targets.forEach(line => {
      const height =
        window.innerHeight / 1.15 - line.getBoundingClientRect().top
      line.style.height = height < 1 ? 0 : height + 'px'
    })
  }

  checkIntersecting() {
    const linePosition = window.innerHeight / 1.15
    this.circles.forEach(circle => {
      if (circle.getBoundingClientRect().top <= linePosition) {
        circle.classList.add('is-active')
      } else {
        circle.classList.remove('is-active')
      }
    })
  }
}

import Swiper from 'swiper/dist/js/swiper'

export default class {
  constructor() {
    this.target = document.getElementById('js-mv-news-swiper')
    this.slider = null
    this.mql = window.matchMedia('only screen and (max-width: 767px)')
  }

  init() {
    this.createSlider()
  }

  createSlider() {
    this.slider = new Swiper(this.target, {
      init: false,
      loop: true,
      effect: 'fade',
      slidesPerView: 1,
      spaceBetween: 0,
      navigation: {
        prevEl: '.mv-news-swiper-button-prev',
        nextEl: '.mv-news-swiper-button-next'
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      }
    })
    this.slider.init()
  }
}

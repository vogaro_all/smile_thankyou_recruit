import Swiper from 'swiper/dist/js/swiper'
import 'intersection-observer'

export default class {
  constructor() {
    this.target = document.getElementById('js-mv-swiper')
    this.slider = null
    this.mql = window.matchMedia('only screen and (max-width: 767px)')
    this.typos = document.querySelectorAll('[js-mv-typo]')
    this.sliderWrap = document.getElementById('js-mv-slider')
  }

  init() {
    this.createSlider()
    this.onSlideChange()
    this.handleEvents()
    // this.isDisplayByTarget()
  }

  handleEvents() {
    window.addEventListener('resize', this.onResize.bind(this))
  }

  createSlider() {
    this.slider = new Swiper(this.target, {
      init: false,
      loop: true,
      slidesPerView: 1,
      spaceBetween: 0,
      observer: true,
      observeParents: true,
      effect: 'fade',
      onSlideChangeEnd: s => {
        s.fixLoop()
      },
      autoplay: {
        delay: 4000, // ４秒後に次のスライドへ
        disableOnInteraction: false // ユーザー側で操作してもスライドを止めない
      },
      simulateTouch: false,
      speed: 2000,
      breakpoints: {
        767: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      }
    })
    this.slider.on('slideChange', () => {
      this.onSlideChange()
    })
    this.slider.update()
    this.slider.init()
  }

  onSlideChange() {
    const currentSlideIndex = this.slider.realIndex
    if (this.slider) {
      this.changeClassOfTypo(currentSlideIndex)
    }
  }

  changeClassOfTypo(idx) {
    this.typos.forEach(typo => {
      typo.classList.remove('is-show')
    })
    this.typos[idx].classList.add('is-show')
  }

  onResize() {
    this.slider.update()
  }

  // isDisplayByTarget() {
  //   const mv = document.getElementById('js-mv')
  //   const footerArea = document.getElementById('js-footer-message')
  //   const observer = new IntersectionObserver(this.onIntersect.bind(this))
  //   observer.observe(mv)
  //   observer.observe(footerArea)
  // }

  // onIntersect(entries) {
  //   if (!this.slider) return
  //   const isIntersecting = entries.some(entry => entry.isIntersecting)

  //   if (isIntersecting) {
  //     this.slider.autoplay.start()
  //   } else {
  //     this.slider.autoplay.stop()
  //   }
  // }
}

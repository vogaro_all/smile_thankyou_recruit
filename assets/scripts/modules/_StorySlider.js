import Swiper from 'swiper/dist/js/swiper'
import 'intersection-observer'

export default class {
  constructor() {
    this.wrapper = document.querySelector('.sec-story')
    this.target = document.getElementById('js-story-swiper')
    this.slider = null
  }

  init() {
    this.createSlider()
    this.addPaginationbarStyle()
    this.isDisplayByTarget()
  }

  createSlider() {
    this.slider = new Swiper(this.target, {
      init: false,
      loop: true,
      slidesPerView: 1,
      effect: 'fade',
      onSlideChangeEnd: s => {
        s.fixLoop()
      },
      pagination: {
        el: '.story-swiper-pagination-md',
        clickable: true,
        type: 'bullets',
        renderBullet: function(index, className) {
          return (
            '<div class="' +
            className +
            '"><span class="line" js-pagenation-target><span class="line__child" js-line-target></span></div>'
          )
        }
      },
      autoplay: {
        delay: 4000, // ４秒後に次のスライドへ
        disableOnInteraction: false
      },
      speed: 2000,
      breakpoints: {
        767: {
          slidesPerView: 1,
          spaceBetween: 0,
          pagination: {
            el: '.story-swiper-pagination',
            type: 'fraction',
            renderFraction: function(currentClass) {
              return `<div class="story-pagenation__current">
              <span class="story-pagenation__line" js-pagenation-target>
                <span class="line-child" js-line-target></span>
              </span>
              <div class="story-pagenation__txts">
                EPISODE 0
                <span class="story-pagenation__current-num ${currentClass}"></span>
              </div>
              </div>`
            }
          },
          navigation: {
            prevEl: '.story-swiper-button-prev',
            nextEl: '.story-swiper-button-next'
          }
        }
      }
    })
    this.slider.on('slideChange', () => {
      this.addPaginationbarStyle()
    })
    this.slider.init()
  }

  addPaginationbarStyle() {
    this.wrapper.classList.remove('is-animation')
    this.wrapper.offsetHeight // eslint-disable-line
    this.wrapper.classList.add('is-animation')
  }

  isDisplayByTarget() {
    const storyArea = document.getElementById('js-sec-story')
    const observer = new IntersectionObserver(this.onIntersect.bind(this))
    observer.observe(storyArea)
  }

  onIntersect(entries) {
    if (!this.slider) return
    const isIntersecting = entries.some(entry => entry.isIntersecting)
    const targetLines = document.querySelectorAll('[js-line-target]')
    if (isIntersecting) {
      targetLines.forEach(line => {
        line.style.animationPlayState = 'running'
      })
      this.slider.autoplay.start()
    } else {
      targetLines.forEach(line => {
        line.style.animationPlayState = 'paused'
      })

      this.slider.autoplay.stop()
    }
  }
}

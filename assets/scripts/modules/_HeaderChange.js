export default class {
  constructor(value) {
    this.value = value
    this.target = document.getElementById('header')
  }

  init() {
    window.addEventListener('load', () => {
      console.log(this.value.getBoundingClientRect().top + window.pageYOffset)

      if (
        window.pageYOffset >=
        this.value.getBoundingClientRect().top + window.pageYOffset
      ) {
        console.log('active')
        this.target.classList.add('is-style-change')
      } else {
        console.log('none-active')
        console.log(window.pageYOffset)
        console.log(this.value.getBoundingClientRect().top + window.pageYOffset)
      }
    })
  }
}

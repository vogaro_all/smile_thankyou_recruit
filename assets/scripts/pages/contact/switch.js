const initialize = function(select) {
  const triggerField = select.getAttribute('data-switching-trigger')
  const triggers = document.querySelectorAll('[name=' + triggerField + ']')
  if (triggers) {
    Array.prototype.forEach.call(triggers, function(trigger) {
      trigger.addEventListener('change', function() {
        initOptions(select, getIndex(triggerField), true)
      })
    })
    initOptions(select, getIndex(triggerField), false)
  }
}

const initOptions = function(select, group, resetSelected) {
  const options = select.querySelectorAll('option')
  Array.prototype.forEach.call(options, function(option) {
    // 選択肢の表示/非表示切り替え
    var active = true
    if (option.hasAttribute('data-switching-group')) {
      if (option.getAttribute('data-switching-group') !== group) {
        active = false
      }
    }
    active ? displayOption(option) : hideOption(option)
  })

  // 選択済み状態の解除
  if (resetSelected) {
    select.querySelector('option:checked').selected = false
  }
}

const displayOption = function(option) {
  const parent = option.parentNode
  if (parent.nodeName.toLowerCase() === 'span') {
    while (parent.firstChild) {
      parent.parentNode.insertBefore(parent.firstChild, parent)
    }
    parent.remove()
  }
}

const hideOption = function(option) {
  const wrapper = document.createElement('span')
  wrapper.style.display = 'none'
  option.parentNode.insertBefore(wrapper, option)
  wrapper.appendChild(option)
}

const getIndex = function(triggerField) {
  const target = document.querySelector('[name=' + triggerField + ']')
  const targetType = target.nodeName.toLowerCase()

  var elem
  if (targetType === 'input' && target.getAttribute('type') === 'radio') {
    elem = document.querySelector('[name=' + triggerField + ']:checked')
  }
  if (targetType === 'select') {
    elem = document.querySelector(
      '[name=' + triggerField + '] option:not([value=""]):checked'
    )
  }

  return elem ? elem.getAttribute('data-index') : null
}

const elems = document.querySelectorAll('select[data-switching-trigger]')
Array.prototype.forEach.call(elems, function(select) {
  initialize(select)
})

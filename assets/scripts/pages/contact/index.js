/* global $ */
;(function() {
  window.addEventListener(
    'load',
    function() {
      const forms = document.getElementsByClassName('needs-validation')
      Array.prototype.filter.call(forms, function(form) {
        form.addEventListener(
          'submit',
          function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault()
              event.stopPropagation()

              // const errorElements = document.querySelectorAll(
              //   'input.form-control:invalid'
              // )
              // const nodes = Array.prototype.slice.call(errorElements, 0)

              $('html, body').animate(
                {
                  scrollTop: $('#error-notice-line').offset().top
                },
                400
              )
            }
            form.classList.add('was-validated')
          },
          false
        )
      })
    },
    false
  )
  if ($('.js-autokana-name').length) {
    $(document).ready(function() {
      $.fn.autoKana('.js-autokana-name', '.js-autokana-kana', {
        katakana: true
      })
    })
  }
  if ($('#error-notice-line').length) {
    $('html, body').animate(
      {
        scrollTop: $('#error-notice-line').offset().top - $('header').height()
      },
      400
    )
  }

  function switchGroup() {
    switch ($('[name=vfCategory]:checked').val()) {
      case 'エントリー':
        $('.form-group-0').show()
        $('.form-group-1').hide()
        break
      case '資料請求':
        $('.form-group-0').hide()
        $('.form-group-1').show()
        break
    }
  }
  $('#vfCategory_0, #vfCategory_1').click(switchGroup)
  switchGroup()
})()

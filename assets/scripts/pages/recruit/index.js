// import Tab from '../../modules/_Tab'
import Scroller from '../../modules/_scroller'

// const tab = new Tab('#js-tab')
// tab.init()

// scroller
const scroller = new Scroller('.js-scroller')
scroller.init()

// URLパラメータによってアクティブのタブを切り替える
// {
//   function getUrlPram() {
//     const URL = location.search.substring(1)
//     return URL
//   }

//   function tabActive() {
//     const PARAM = getUrlPram()
//     if (!PARAM) return
//     const TAB_ROOT = document.querySelector('#js-tab')
//     const TRIGGERS = [...TAB_ROOT.querySelectorAll('[data-tab-target]')]
//     if (PARAM === 'current-tab-01') {
//       tab.checkCurrentTab(TRIGGERS[0])
//     } else {
//       tab.checkCurrentTab(TRIGGERS[1])
//     }
//   }

//   tabActive()
// }

{
  function getGtmListner() {
    const triggers = [...document.querySelectorAll('[js-gtm-trigger]')]
    triggers.forEach(trigger => {
      onClick(trigger)
    })

    function onClick(trigger) {
      trigger.addEventListener('click', () => {
        if (trigger.getAttribute('aria-expanded') === 'true') return
        const GET_FAQ_NAME = trigger.querySelector('[js-gtm-trigger-child]')
          .innerText
        dataLayer.push({ // eslint-disable-line
          event: 'accordion_open',
          question: GET_FAQ_NAME
        })
      })
    }
  }

  getGtmListner()
}

import 'intersection-observer'
import Loading from '../../modules/_Loading'

{
  const config = {
    targetText: 'sec-give-word'
  }
  const param = [...location.href.substring(1).split('/#')]
  const target = document.getElementById('header')
  if (param[1] === config.targetText) {
    target.classList.add('is-style-change')
  }
}

// Loading

{
  const page = document.querySelector('[data-page]')

  if (page) {
    const smImgPaths = [
      `/assets/images/pages/message/${page.dataset.page}/content_bg01.jpg`
    ]

    const mdImgPaths = [
      `/assets/images/pages/message/${page.dataset.page}/content_bg01_md.jpg`
    ]

    const mql = window.matchMedia('only screen and (max-width: 767px)')

    const paths = mql.matches ? smImgPaths : mdImgPaths
    const loading = new Loading(paths)
    loading.init()
  }
}

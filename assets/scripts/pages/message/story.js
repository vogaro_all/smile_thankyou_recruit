import 'intersection-observer'
import Loading from '../../modules/_Loading'

{
  const targets = [...document.querySelectorAll('.js-scroll-object')]
  const observer = new IntersectionObserver(onIntersect, {
    rootMargin: '0px',
    threshold: 0
  })
  const imgs = [...document.querySelectorAll('.js-scroll-image')]
  targets.forEach(target => {
    const marker = document.createElement('div')
    marker.style.position = 'absolute'
    marker.style.width = '100%'
    marker.style.height = '1px'
    marker.classList.add('marker')
    target.appendChild(marker)
    observer.observe(marker)
    if (marker.getBoundingClientRect().top < 0) {
      targets.forEach(target => {
        target.classList.remove('is-active')
      })

      imgs.forEach(img => {
        img.classList.remove('is-active')
      })

      const index = targets.indexOf(target)
      imgs[index].classList.add('is-active')
      target.classList.add('is-active')
    }
  })

  function onIntersect(entries) {
    entries.forEach(entry => {
      if (!entry.isIntersecting) return
      const target = entry.target.parentNode
      const index = targets.indexOf(target)

      targets.forEach(target => {
        target.classList.remove('is-active')
      })

      imgs.forEach((img, i) => {
        if (i >= index) {
          img.classList.remove('is-active')
        }
      })

      target.classList.add('is-active')
      imgs[index].classList.add('is-active')
    })
  }
}

// Loading

{
  const page = document.querySelector('[data-page]')

  const smImgPaths = [
    `/assets/images/pages/message/${page.dataset.page}/content_bg01.jpg`
  ]

  const mdImgPaths = [
    `/assets/images/pages/message/${page.dataset.page}/content_bg01_md.jpg`
  ]

  const mql = window.matchMedia('only screen and (max-width: 767px)')

  const paths = mql.matches ? smImgPaths : mdImgPaths
  const loading = new Loading(paths)
  loading.init()
}

import 'intersection-observer'
import _ from 'lodash'
import MvNews from '../modules/_MvNewsSlider'
import StorySlider from '../modules/_StorySlider'
import MvSlider from '../modules/_MvSlider'
import { TweenLite, Power4 } from 'gsap/all'
import Loading from '../modules/_Loading'
import picturefill from 'picturefill'
import Stickyfill from 'stickyfilljs'

window.onload = () => {
  const MvNewsSlider = new MvNews()
  MvNewsSlider.init()

  const storySlider = new StorySlider()
  storySlider.init()
}

// picturefill stickyfill
{
  const mql = window.matchMedia('only screen and (min-width: 767px)')
  const target = document.getElementById('js-sticky')
  Stickyfill.add(target)

  const fill = () => {
    picturefill()
  }
  mql.addListener(fill)
  fill()
}

{
  const mql = window.matchMedia('(max-width: 767.98px)')
  // *
  // SP時にnews記事を4記事にする
  // *
  function checkBreakPoint(mql) {
    function _hideStyle(target) {
      target.style.display = 'none'
    }

    const targets = document.querySelectorAll('[js-index-news]')
    function _cheackTargetCount() {
      for (let i = 0; targets.length > i; i++) {
        if (i > 3) {
          _hideStyle(targets[i])
        }
      }
    }
    if (mql.matches) {
      _cheackTargetCount()
    } else {
      targets.forEach(target => {
        target.style.display = 'block'
      })
    }
  }

  function setHeight(mql) {
    const target = document.getElementById('js-mv')
    function getDeviceInnerHeight() {
      return window.innerHeight
    }
    if (mql.matches) {
      target.style.height = `${getDeviceInnerHeight()}px`
    } else {
      target.style.height = ''
    }
  }

  // ブレイクポイントの瞬間に発火
  mql.addListener(checkBreakPoint)
  mql.addListener(setHeight)

  // 初回チェック
  checkBreakPoint(mql)
  setHeight(mql)
}

function createAnimations(id, _speed) {
  const el = document.getElementById(id)

  const maskPaths = el.querySelectorAll('path')
  const speed = _speed || 600
  let delay = 0

  _.forEach(maskPaths, el => {
    const pathLength = el.getTotalLength()
    const duration = pathLength / speed
    el.style.opacity = '1'

    el.style.strokeDasharray = pathLength
    el.style.strokeDashoffset = pathLength

    TweenLite.to(el, duration, {
      delay: delay,
      ease: Power4.easeInOut,
      strokeDashoffset: 0
    })
  })
}

function checkIsIntersected(el) {
  const target = document.querySelector(`.${el}`)

  const observer = new MutationObserver(records => {
    createAnimations('svg-animation', 500)
  })
  observer.observe(target, {
    attributes: true
  })
}

checkIsIntersected('story-heading-wrap__ttl')

// Loading

{
  // mv-slider
  const smImgPaths = [
    '/assets/images/pages/index/mv_slider_item01.jpg',
    '/assets/images/pages/index/mv_slider_item02.jpg'
  ]

  const mdImgPaths = [
    '/assets/images/pages/index/mv_slider_item01_md.jpg',
    '/assets/images/pages/index/mv_slider_item02_md.jpg'
  ]

  const mql = window.matchMedia('only screen and (max-width: 767px)')

  const paths = mql.matches ? smImgPaths : mdImgPaths

  const mvSlider = new MvSlider()
  const loading = new Loading(paths, () => {
    mvSlider.init()
  })
  loading.init()
}

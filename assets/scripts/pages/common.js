import 'intersection-observer'
import Collapse from '../modules/_Collapse'
import FooterSlider from '../modules/_FooterSlider'
import HeaderChange from '../modules/_HeaderChange'
import 'picturefill'
import axios from 'axios'
import _ from 'lodash'

// collapse
window.addEventListener('load', () => {
  _.forEach(document.querySelectorAll('[data-collapse-toggler]'), el => {
    const collapse = new Collapse(el, {
      hashNavigation: true
    })
    collapse.init()
  })
})

{
  const mql = window.matchMedia('(max-width: 767.98px)')

  // smのメニュー展開時にjs-get-hightに対してデバイスの高さ当てる

  function setHeight(mql) {
    const target = document.querySelector('[js-get-hight]')
    function getDeviceInnerHeight() {
      return window.innerHeight - 60
    }
    if (mql.matches) {
      target.style.height = `${getDeviceInnerHeight()}px`
    } else {
      target.style.height = ''
    }
  }

  function setHeaderScrollX(mql) {
    const target = document.getElementById('header')

    function scroll() {
      let scrollLeft = window.scrollX || window.pageXOffset

      if (!mql.matches) {
        target.style.transform = `translate3d(${-scrollLeft}px , 0, 0)`
        target.style.transform = `-webkit-transform3d(${-scrollLeft}px , 0, 0)`
      } else {
        target.style.transform = ''
      }
    }

    window.addEventListener('load', () => {
      scroll()
    })
  }

  // ブレイクポイントの瞬間に発火
  mql.addListener(setHeight)
  mql.addListener(setHeaderScrollX)

  // 初回チェック
  setHeight(mql)
  setHeaderScrollX(mql)

  // menu-sp
  const ACTIVE = 'is-active'

  const toggler = document.getElementById('js-header-toggler')
  const target = document.getElementById('js-header-target')
  toggler.addEventListener('click', () => {
    if (toggler.classList.contains(ACTIVE)) {
      document.documentElement.style.overflow = 'auto'
      toggler.classList.remove(ACTIVE)
      target.classList.remove(ACTIVE)
    } else {
      setHeight(mql)
      document.documentElement.style.overflow = 'hidden'
      toggler.classList.add(ACTIVE)
      target.classList.add(ACTIVE)
    }
  })
}

// ScrollAnimationTrigger
{
  const options = {
    threshold: [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
    rootMargin: '-20px'
  }

  const getThreshold = str => {
    if (!str) return options.threshold

    if (~str.indexOf(',')) {
      return str.split(',').map(Number)
    } else {
      return [Number(str)]
    }
  }

  class ScrollAnimationTrigger {
    constructor(el) {
      this._el = el
      this._observer = null
    }

    init() {
      this._options = _.merge({}, options)
      this._options.threshold = getThreshold(this._el.dataset.threshold)
      this._observer = new IntersectionObserver(
        this._onIntersection.bind(this),
        this._options
      )
      this._observer.observe(this._el)
    }

    _onIntersection(changes) {
      changes.forEach(c => {
        const target = c.target
        const isIntersecting = c.isIntersecting

        if (isIntersecting) {
          this._observer.unobserve(target)
          this._el.classList.add('is-intersected')
        }
      })
    }
  }

  const scrollAnimationEls = document.querySelectorAll('.js-io-element')

  _.each(scrollAnimationEls, el => {
    const scrollAnimationTrigger = new ScrollAnimationTrigger(el)
    scrollAnimationTrigger.init()
  })
}

const url = location.href

{
  function createMarker() {
    const marker = document.createElement('div')
    marker.style.position = 'absolute'
    marker.style.width = '100%'
    marker.style.height = '1px'
    marker.style.pointerEvents = 'none'

    return marker
  }

  // ストーリーページ
  const target = document.querySelector('[data-header-check]')
  if (target) {
    target.style.position = 'relative'
  }
  const topMarker = createMarker()
  topMarker.classList.add('top-marker')

  const bottomMarker = createMarker()
  bottomMarker.classList.add('bottom-marker')

  if (target) {
    topMarker.style.top = '-90px'
    bottomMarker.style.top = 'calc(100vh - 120px)'
    target.appendChild(topMarker)
    target.appendChild(bottomMarker)
  } else {
    topMarker.style.top = '0'
    bottomMarker.style.top = 'calc(100vh + 120px)'
    document.body.appendChild(topMarker)
    document.body.appendChild(bottomMarker)
  }
  // ここまで

  const headerChange = new HeaderChange(bottomMarker)
  headerChange.init()

  const observer = new IntersectionObserver(onIntersect)
  const headerElement = document.getElementById('header')

  observer.observe(topMarker)
  observer.observe(bottomMarker)

  if (bottomMarker.getBoundingClientRect().top < 0) {
    headerElement.classList.add('is-style-change')
  }

  function onIntersect(entries) {
    entries.forEach(entry => {
      if (!entry.isIntersecting) return

      if (entry.target.classList.contains('top-marker')) {
        headerElement.classList.remove('is-style-change')
      } else {
        headerElement.classList.add('is-style-change')
      }
    })
  }
}

if (!url.match('/story')) {
  const header = document.getElementById('header')
  const headerH = header.clientHeight // headerの高さ
  let startPos = 0 // 開始位置

  const headerFnc = () => {
    let currentPos = document.documentElement.scrollTop
    if (currentPos > startPos && currentPos > headerH) {
      // 下スクロールしているとき
      header.classList.add('is-style-change')
    }
    startPos = currentPos
  }

  window.addEventListener('scroll', _.throttle(headerFnc, 350)) // 0.35秒ごとに呼ばれる
}

window.onload = () => {
  // mv-slider
  const footerSlider = new FooterSlider()
  footerSlider.init()
}

// 375px以下でviewportを固定
{
  const minWidth = 375
  const viewportEl = document.querySelector('meta[name="viewport"]')
  const mediaQueryList = window.matchMedia(`(min-device-width: ${minWidth}px)`)

  function onChange() {
    const viewportContent = mediaQueryList.matches
      ? 'width=device-width, initial-scale=1'
      : `width=${minWidth}`

    viewportEl.setAttribute('content', viewportContent)
  }

  mediaQueryList.addListener(onChange)
  onChange()
}

// DeviceFullHeight
{
  const target = document.querySelectorAll('.js-device-full-height')

  function heightSet() {
    _.forEach(target, el => {
      el.style.height = `${document.documentElement.clientHeight}px`
    })
  }

  heightSet()
  window.addEventListener('resize', heightSet)
}

;(async () => {})()

class FooterWirteStaffJson {
  constructor() {
    this.targets = [...document.querySelectorAll('[js-json-footer]')]
  }

  async getJson() {
    const res = await axios.get('/data/staff.json')

    this.createElement(res.data)
  }

  createElement(staffDates) {
    this.targets.forEach((target, index) => {
      staffDates.forEach((data, index) => {
        let createdElement = document.createElement('a')
        createdElement.href = data.pageLink
        createdElement.innerHTML = `- ${data.name}`
        createdElement.classList.add('footer-accordion__link')
        let createdParent = document.createElement('div')
        createdParent.classList.add('footer-accordion__ls')
        createdParent.appendChild(createdElement)
        target.appendChild(createdParent)
      })
    })
  }
}

const footerwirteStaffJson = new FooterWirteStaffJson()
footerwirteStaffJson.getJson()

class HeaderWirteStaffJson {
  constructor() {
    this.target = document.querySelector('[js-json-header]')
  }

  async getJson() {
    const res = await axios.get('/data/staff.json')

    this.createElement(res.data)
  }

  createElement(staffDates) {
    staffDates.forEach((data, index) => {
      let createdElement = document.createElement('a')
      createdElement.href = data.pageLink
      createdElement.innerHTML = `- ${data.name}`
      createdElement.classList.add(
        'header-accordion__link',
        'header-accordion__link--staff'
      )
      let createdParent = document.createElement('div')
      createdParent.classList.add('header-accordion__ls')
      createdParent.appendChild(createdElement)
      this.target.appendChild(createdParent)
    })
  }
}

const headerWirteStaffJson = new HeaderWirteStaffJson()
headerWirteStaffJson.getJson()

// ieでスクロールカクツク問題解消
if (
  navigator.userAgent.match(/MSIE 10/i) ||
  navigator.userAgent.match(/Trident\/7\./)
) {
  const body = document.getElementsByTagName('body')
  body[0].addEventListener('mousewheel', () => {
    event.preventDefault()
    let wd = event.wheelDelta
    let csp = window.pageYOffset
    window.scrollTo(0, csp - wd)
  })
}

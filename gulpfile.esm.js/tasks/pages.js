import gulp from 'gulp'
import gulpLoadPlugins from 'gulp-load-plugins'
import fs from 'fs'
import * as utils from '../utils'
import common from '../../common'
import config from '../../config'

const $ = gulpLoadPlugins()
const isDev = process.env.NODE_ENV === 'development'

export default function pages() {
  return (
    gulp
      .src(common.srcPaths.pages, {
        base: `${config.srcDir}/${common.dir.pages}`
      })
      .pipe(
        $.if(
          isDev,
          $.plumber({
            errorHandler: $.notify.onError()
          })
        )
      )
      .pipe(
        $.ejs(
          {
            data: JSON.parse(
              fs.readFileSync(
                `${config.srcDir}/static/${common.dir.data}/staff.json`
              )
            )
          },
          {
            root: `${config.srcDir}/${common.dir.includes}`
          },
          {
            ext: '.shtml'
          }
        )
      )
      // .pipe($.htmlhint('.htmlhintrc'))
      // .pipe($.htmlhint.reporter())
      .pipe(
        $.htmlhint.failOnError({
          suppress: true
        })
      )
      .pipe(
        $.if(
          !isDev,
          $.htmlmin({
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            decodeEntities: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeComments: false,
            removeEmptyAttributes: false,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            trimCustomFragments: true,
            useShortDoctype: true
          })
        )
      )
      .pipe($.replace(/@img-index/g, `/assets/images/pages/index`))
      .pipe($.replace(/@img-common/g, `/assets/images/pages/common`))
      .pipe($.replace(/@img-message/g, `/assets/images/pages/message`))
      .pipe($.replace(/@img-staff/g, `/assets/images/pages/staff`))
      .pipe($.replace(/@img-company/g, `/assets/images/pages/company`))
      .pipe($.replace(/@img-growth/g, `/assets/images/pages/growth`))
      .pipe($.replace(/@img-recruit/g, `/assets/images/pages/recruit`))
      .pipe($.replace(/@style/g, `/assets/styles/`))
      .pipe($.replace(/@scripts/g, `/assets/scripts/`))
      .pipe(utils.detectConflict())
      .pipe(gulp.dest(`${config.distDir}/${config.baseDir}`))
      .pipe($.if(config.gzip && !isDev, $.gzip()))
      .pipe($.if(config.gzip && !isDev, utils.detectConflict()))
      .pipe(
        $.if(
          config.gzip && !isDev,
          gulp.dest(`${config.distDir}/${config.baseDir}`)
        )
      )
      .pipe(common.server.stream())
  )
}

<header class="l-header" id="header">
  <div class="l-header__head" id="js-header-target">
    <p class="l-header__logo">
      <a href="/">
        <svg class="icon icon--logo" width="300" height="58"><use xlink:href="#svg-logo-icon"></use></svg>
      </a>
    </p>
    <button class="l-header__button" id="js-header-toggler">
      <span class="line">メニューを開く</span>
    </button>
  </div>
  <nav class="l-header__menu" js-get-hight>
    <div class="l-header__menu-inner">
      <ul class="menu-list">
        <li class="menu-list__item u-d-md-none"><a href="/">トップページ</a></li>
        <li class="menu-list__item"><a href="/company/">会社紹介</a></li>
        <li class="menu-list__item"><a href="/message/">採用メッセージ</a></li>
        <li class="menu-list__item"><a href="/growth/">成長環境</a></li>
        <li class="menu-list__item"><a href="/growth/training/">研修制度</a></li>
        <li class="menu-list__item menu-list__item--button">
          <a href="/staff/" class="u-d-none u-d-md-block">スタッフ紹介</a>
          <div class="u-d-md-none">
            <button class="header-acc-btn" id="h-acc-toggler-01" aria-controls="h-acc-collapsible-01" data-collapse-toggler>スタッフ紹介</button>
            <div class="header-accordion" id="h-acc-collapsible-01" aria-labelledby="h-acc-collapse-toggler-01">
              <div class="header-accordion__wrap" js-json-header>
                <div class="header-accordion__ls">
                  <a href="/staff/" class="header-accordion__link header-accordion__link--staff">
                    - スタッフ一覧
                  </a>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li class="menu-list__item"><a href="/recruit/graduate.shtml">採用情報</a></li>
      </ul>
      <div class="head-sns">
        <p class="head-sns__ttl">
          ソーシャルアカウント
        </p>
        <ul class="head-sns__wrap">
          <li class="head-sns__ls">
            <a href="https://twitter.com/smile39_saiyou" class="head-sns__link" target="_blank" id="H_SNS_TW">
              <svg class="icon icon--twitter"><use xlink:href="#svg-sns-icon02"></use></svg>
            </a>
          </li>
          <li class="head-sns__ls">
            <a href="https://www.instagram.com/smileandthankyou/?hl=ja" class="head-sns__link" target="_blank" id="H_SNS_IG">
              <svg class="icon icon--insta"><use xlink:href="#svg-sns-icon01"></use></svg>
            </a>
          </li>
          <li class="head-sns__ls">
            <a href="https://www.facebook.com/smile39saiyou" class="head-sns__link" target="_blank" id="H_SNS_FB">
              <svg class="icon icon--facebook"><use xlink:href="#svg-sns-icon03"></use></svg>
            </a>
          </li>
        </ul>
      </div>
      <ul class="entry-button">
        <li class="entry-button__ls">
          <a href="/recruit/entry/graduate/" class="c-button--green entry-button__item" id="H_N_ENTRY">
            <span class="u-d-none u-d-md-block">新卒採用エントリー</span>
            <span class="u-d-md-none">
              新卒採用<br>
              <span class="en">
                ENTRY
              </span>
            </span>
          </a>
        </li>
        <li class="entry-button__ls">
          <a href="/recruit/entry/career/" class="c-button--orange entry-button__item" id="H_M_ENTRY">
            <span class="u-d-none u-d-md-block">中途採用エントリー</span>
            <span class="u-d-md-none">
              中途採用<br>
              <span class="en">
                ENTRY
              </span>
            </span>
          </a>
        </li>
      </ul>
    </div>
  </nav><!-- .l-header__menu -->
</header>

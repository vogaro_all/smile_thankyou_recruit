<footer class="l-footer">
  <div class="footer">
    <div class="cv">
      <div class="l-container">
        <div class="cv__wrapper">
          <div class="cv__heading">
            <h2 class="c-main-ttl--center js-io-element">
              <span class="c-main-ttl__color01">RECRUIT</span><span class="c-main-ttl__color02">INFO</span><br>
              <span class="c-main-ttl__jp">採用情報</span>
            </h2>
          </div>
          <div class="cv__content">
            <div class="cv__button">
              <a href="/recruit/graduate.shtml" class="c-button--arrow c-button--graduate"><p><span>新卒採用</span><br>募集要項を見る</p></a>
            </div>
            <div class="cv__button">
              <a href="/recruit/career.shtml" class="c-button--arrow c-button--career"><p><span>中途採用</span><br>募集要項を見る</p></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-message" id="js-footer-message">
      <div class="footer-slider" id="js-footer-swiper">
        <div class="swiper-wrapper footer-slider__wrapper">
          <div class="swiper-slide footer-slider__slide">
            <div class="footer-slider__img--01"></div>
          </div>

          <div class="swiper-slide footer-slider__slide">
            <div class="footer-slider__img--02"></div>
          </div>

          <div class="swiper-slide footer-slider__slide">
            <div class="footer-slider__img--03"></div>
          </div>
        </div>
      </div>
      <div class="footer-message__wrap">
        <h2 class="footer-message__ttl js-io-element show-animation">
          <!-- sp -->
          <span class="u-d-md-none"><span class="footer-message__acc">医療家の人生がはじまる</span><br><span
              class="footer-message__acc">広がる可能性に</span><br><span class="footer-message__acc">きっとあなたは驚く</span></span>
          <!-- pc -->
          <span class="u-d-none u-d-md-inline-block"><span class="footer-message__acc">医療家の人生がはじまる</span><br><span class="footer-message__acc">広がる可能性にきっとあなたは驚く</span></span>
        </h2>
        <p class="footer-message__lead js-io-element show-animation">
          当社は患者様の体と心に安らぎを与え、世界一患者様を幸せにすることを目指しています。<br class="u-d-none u-d-md-block">ともに働く仲間と多くの感動を共有し、あなた自身の夢を叶えて欲しいと願っております。<br class="u-d-none u-d-md-block">あなたが医療家として輝ける環境、やりがいのある仕事がここにあります。
        </p>
      </div>
    </div>
    <div class="footer-entry">
      <div class="l-container--wide">
        <ul class="footer-entry__wrapper">
          <li class="footer-entry__ls">
            <a href="/recruit/entry/graduate/" class="footer-entry-button--new-graduates" id="F_N_ENTRY">
              <h3 class="footer-entry-button__ttl">
                新卒採用<br><span class="footer-entry-button__en">ENTRY</span>
              </h3>
              <svg class="icon icon--arrow" width="15" height="9">
                <use xlink:href="#svg-arrow-icon"></use>
              </svg>
            </a>
            <a href="/recruit/graduate.shtml" class="entry-offering"><span class="entry-offering__arrow"><svg class="icon icon--arrow" width="15" height="9"><use xlink:href="#svg-arrow-icon"></use></svg></span>新卒採用の<br class="u-d-md-none">募集要項を見る</a>
          </li>
          <li class="footer-entry__ls">
            <a href="/recruit/entry/career/" class="footer-entry-button--mid-career" id="F_M_ENTRY">
              <h3 class="footer-entry-button__ttl">
                中途採用<br><span class="footer-entry-button__en">ENTRY</span>
              </h3>
              <svg class="icon icon--arrow" width="15" height="9">
                <use xlink:href="#svg-arrow-icon"></use>
              </svg>
            </a>
            <a href="/recruit/career.shtml" class="entry-offering"><span class="entry-offering__arrow"><svg class="icon icon--arrow" width="15" height="9"><use xlink:href="#svg-arrow-icon"></use></svg></span>中途採用の<br class="u-d-md-none">募集要項を見る</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="footer-body">
      <div class="l-container">
        <div class="footer-body__wrapper">
          <ul class="footer-item">
            <ls class="footer-item__ls">
              <a href="#" class="footer-item__link u-d-md-none">
                トップページ
              </a>
              <a href="/company/" class="footer-item__link">
                会社紹介
              </a>
              <a href="/message/" class="footer-item__link">
                採用メッセージ
              </a>
              <a href="/growth/" class="footer-item__link">
                成長環境
              </a>
              <a href="/growth/training/" class="footer-item__link">
                研修制度
              </a>
            </ls>
            <ls class="footer-item__ls footer-item__ls--accordion">
              <a href="/staff/" class="footer-item__link footer-item__link--button u-d-md-block u-d-none" id="acc-toggler-01" aria-controls="acc-collapsible-01" data-collapse-toggler>スタッフ紹介<span class="f-toggle-line"></span>
              </a>
              <button class="footer-item__link u-d-md-none footer-item__link--button" id="acc-toggler-01" aria-controls="acc-collapsible-01" data-collapse-toggler>スタッフ紹介<span class="f-toggle-line"></span>
              </button>
              <div class="footer-accordion u-d-md-none" id="acc-collapsible-01"
                aria-labelledby="acc-collapse-toggler-01">
                <div class="footer-accordion__wrap" js-json-footer>
                  <div class="footer-accordion__ls u-d-md-none">
                    <a href="/staff/" class="footer-accordion__link">- スタッフ一覧</a>
                  </div>
                </div>
              </div>
              <div class="footer-accordion u-d-md-block u-d-none">
                <div class="footer-accordion__wrap" js-json-footer></div>
              </div>
            </ls>
            <ls class="footer-item__ls footer-item__ls--accordion">
              <a href="/message/#sec-give-word" class="footer-item__link footer-item__link--button u-d-none u-d-md-block">スマサンストーリー<span class="f-toggle-line"></span>
              </a>
              <button class="footer-item__link footer-item__link--button u-d-md-none" id="acc-toggler-02" aria-controls="acc-collapsible-02" data-collapse-toggler>スマサンストーリー<span class="f-toggle-line"></span>
              </button>
              <div class="footer-accordion u-d-md-block u-d-none">
                <div class="footer-accordion__wrap">
                  <div class="footer-accordion__ls">
                    <a href="/message/story01.shtml" class="footer-accordion__link">- スタートラインに立った日</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story02.shtml" class="footer-accordion__link">- 人生で今がいちばん充実</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story03.shtml" class="footer-accordion__link">- 聞きたかった言葉</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story04.shtml" class="footer-accordion__link">- ひと夏の小さな約束</a>
                  </div>
                </div>
              </div>
              <div class="footer-accordion u-d-md-none" id="acc-collapsible-02"
                aria-labelledby="acc-collapse-toggler-02">
                <div class="footer-accordion__wrap">
                  <div class="footer-accordion__ls">
                    <a href="/message/story01.shtml" class="footer-accordion__link">- スタートラインに立った日</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story02.shtml" class="footer-accordion__link">- 人生で今がいちばん充実</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story03.shtml" class="footer-accordion__link">- 聞きたかった言葉</a>
                  </div>
                  <div class="footer-accordion__ls">
                    <a href="/message/story04.shtml" class="footer-accordion__link">- ひと夏の小さな約束</a>
                  </div>
                </div>
              </div>
            </ls>
            <ls class="footer-item__ls">
              <a href="/recruit/graduate.shtml" class="footer-item__link">
                採用情報
              </a>
              <a href="/recruit/entry/graduate/" class="footer-item__link" id="F_MN_ENTRY">
                新卒採用エントリー
              </a>
              <a href="/recruit/entry/career/" class="footer-item__link" id="F_MM_ENTRY">
                中途採用エントリー
              </a>
              <a href="/privacy/" class="footer-item__link">
                プライバシーポリシー
              </a>
            </ls>
            <ls class="footer-item__ls">
              <div class="footer-sns">
                <p class="footer-sns__ttl">
                  ソーシャルアカウント
                </p>
                <ul class="footer-sns__item">
                  <li class="footer-sns__ls">
                    <a href="https://twitter.com/smile39_saiyou" class="footer-sns__link" target="_blank" id="F_SNS_TW">
                      <svg class="icon icon--twitter">
                        <use xlink:href="#svg-sns-icon02"></use>
                      </svg>
                    </a>
                  </li>
                  <li class="footer-sns__ls">
                    <a href="https://www.instagram.com/smileandthankyou/?hl=ja" class="footer-sns__link" target="_blank" id="F_SNS_IG">
                      <svg class="icon icon--insta">
                        <use xlink:href="#svg-sns-icon01"></use>
                      </svg>
                    </a>
                  </li>
                  <li class="footer-sns__ls">
                    <a href="https://www.facebook.com/smile39saiyou" class="footer-sns__link" target="_blank" id="F_SNS_FB">
                      <svg class="icon icon--facebook">
                        <use xlink:href="#svg-sns-icon03"></use>
                      </svg>
                    </a>
                  </li>
                </ul>
              </div>
            </ls>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="l-container--wide">
        <div class="footer-bottom__wrapper">
          <div class="footer-bottom__logo">
            <div class="footer-logo">
              <a href="/" class="footer-logo__icon">
                <svg class="icon icon--f-logo">
                  <use xlink:href="#svg-footer-logo"></use>
                </svg>
              </a>
              <a href="http://www.smile-39.com/" class="footer-logo__to-cp" target="_blank">コーポレートサイト<span><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill-rule="evenodd" fill="#373E42" d="M8 6.999V1H2V-.001h7v7H8zM7 2v7H0V2h7zM1 8h5V3H1v5z" /></svg></span></a>
            </div>
          </div>
          <div class="footer-copyright">
            <p class="footer-copyright__txt">Copyright (c) スマイルアンドサンキュー株式会社 All rights reserved.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php namespace Infra\Http;
/**
 * Class Request
 * @package Infra\Http
 */

class Request
{
	/**
	 * @return bool
	 */
	public static function ua_mac()
	{
		return preg_match('/Macintosh/', $_SERVER['HTTP_USER_AGENT'])? true: false;
	}

	/**
	 * @return bool
	 */
	public static function ua_windows()
	{
		return preg_match('/Windows/', $_SERVER['HTTP_USER_AGENT'])? true: false;
	}

	/**
     * @return int
     */
    public static function ie_version()
    {
        if(!stristr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
            $ver = 0;
        } else {
            preg_match('/MSIE\s([\d.]+)/i', $_SERVER['HTTP_USER_AGENT'], $ver);
            $ver = floor($ver[1]);
        }

        return (int) $ver;
    }
}

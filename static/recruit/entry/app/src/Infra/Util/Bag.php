<?php namespace Infra\Util;

/**
 * 簡易なカバンクラスを用意
 * Class Bag
 * @package Infra\Util
 */
class Bag
{
    protected $_ = array();

    public function __construct(array $_){
        $this->_ = ($_ && is_array($_)) ? $_ : array();
    }

    /**
     * キーの存在チェック
     * @param $key
     * @return bool
     */
    public function has($key){ return array_key_exists($key, $this->_); }

    /**
     * デフォルト値つきゲッター
     * @param $key
     * @param string $default
     * @return mixed|string
     */
    public function get($key, $default = '')
    {
        if($this->has($key)) {
            $_ = $this->_[$key];
            if(!empty($_)){
                return $_;
            }
        }

        return $default;
    }

    /**
     * 純粋な配列を取得
     * @return array
     */
    public function asArray(){ return $this->_; }

    /**
     * 指定したキーを除外した配列を取得
     * @param $keys
     * @return array
     */
    public function except($keys){
        return array_diff_key($this->_, array_fill_keys($keys, ''));
    }

    /**
     * 指定した値を除外した配列を取得
     * @param $values
     * @return array
     */
    public function exceptValue($values){
        return array_diff($this->_, $values);
    }

    /**
     * 指定したキーのみの配列を取得
     * @param $keys
     * @return array
     */
    public function only($keys){
        return array_intersect_key($this->_, array_fill_keys($keys, ''));
    }

    /**
     * 指定した値のみの配列を取得
     * @param $values
     * @return array
     */
    public function onlyValue($values){
        return array_intersect($this->_, $values);
    }
}

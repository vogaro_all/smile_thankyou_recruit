<?php namespace Context\Mail;

use Infra\Mail\Mail;
use Infra\View\Template;

class MailFactory {

    private $fields;
    private $item;

    public function __construct($fields, $item) {
        $this->fields = $fields;
        $this->item = $item;
    }

    public function generate($config) {
        $mail = new Mail();

        //FROM
        $mail->setFrom(array($this->getEmail($config['sender']) => $config['senderName']));

        //TO
        $mail->setTo($this->getEmail($config['receiver']));

        //件名
        $mail->setSubject(Template::render($config['subject'], array(
            'item' => $this->item,
        ), false));

        //本文
        $mail->setBody(Template::render($config['body'], array(
            'item' => $this->item,
        ), false));

        return $mail;
    }

    private function getEmail($target) {
        return isset($this->fields[$target]) ? $this->item[$target] : $target;
    }
}
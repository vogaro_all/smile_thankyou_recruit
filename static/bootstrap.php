<?php
require_once(__DIR__ . "/lacne/news/output/post.php");

use Klein\Klein;
use lacne\core\Request;
use lacne\service\PostView;
use lacne\entity\PostSpecEntity as Spec;

$klein = new Klein();
$twig  = new Twig_Environment(new Twig_Loader_Filesystem('./'));

$klein->respond('GET', '/', function () use ($twig) {
    $postView = new PostView();
    $posts = $postView->getPosts(new Spec(array('limit' => 6)));
    return $twig->render('/index.twig.shtml', compact('posts'));
});

$klein->respond('GET', '/news/', function () use ($twig) {
    $postView = new PostView();
    $spec = new Spec(array(
        'limit'    => 10,
        'page'     => Request::get('page'),
        'category' => Request::get('category'),
    ));
    $posts = $postView->getPosts($spec);
    $pager = $postView->getPager($spec);
    return $twig->render('/news/index.twig.shtml', compact('spec', 'posts', 'pager'));
});

$klein->respond(array('GET', 'POST'), '/news/detail', function ($request, $response) use ($twig) {
    try {
        $postView = new PostView();
        $post = $postView->getPost(Request::get('id'));
        return $twig->render('/news/detail.twig.shtml', compact('post'));
    } catch (Exception $e) {
        $response->redirect('/news/', 302);
    }
});


$klein->dispatch();

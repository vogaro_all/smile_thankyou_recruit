<?php
use lacne\core\Library;
/**
// ------------------------------------------------------------------------
 * Lib_lpo_light.php
 * LPO（簡易版）機能用
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_lpo_light extends Library
{
	/**
	 * Lib_lpo_light constructor.
	 * @param $LACNE
	 * @param $db
	 */
	function Lib_lpo_light($LACNE , $db) {
		parent::__construct($LACNE, $db);
	}
	
}

?>
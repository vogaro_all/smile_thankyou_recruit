<?php

/**
 *-------------------------------------------------------------------------
 *  オプション： 動画アップロード
 *  Lib_media_movie.php
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_UPLOAD_MOVIE" , 1);


class Lib_media_movie extends Lib_media 
{
    
    /** @var array  $filetype  アップロード可能拡張子*/
    //許可する動画ファイルの拡張子を追記する
    var $filetype = array("jpg" , "gif" , "png" , "pdf" , "flv");

    /** @var number  $maxfilesize  アップロード可能なサイズ*/
    //php.ini の upload_max_filesizeの値などに注意。必要であればini側を変更するなどの対策が必要
    var $maxfilesize = 5000; //KB
    
    //動画ファイルを一覧表示させる際の各動画ファイルタイプ別アイコン画像
    var $movie_thumb_arr = array(
        "flv" => "images/thumb_movie_flv.gif",
        "wmv" => "images/thumb_movie_wmv.gif",
        "mov" => "images/thumb_movie_mov.gif"
    );
    
    /** このオプション機能で画面表示利用されるhtmlや画像がどこに格納されているかパスを示しておく */
    var $option_view_path;
    
    
    function Lib_media_movie($LACNE , $db){
        parent::Lib_media($LACNE , $db);
        $this->option_view_path = LACNE::get_path_docroot(realpath(dirname(__FILE__)."/view"));
    }
    
    /**
     * サムネイル表示用画像ファイルのパスを返す
     * @param object $file
     * @return string
     */
    function get_thumbpath($file)
    {
        
        if($file["type"] == "image")
        {
			$file_ext = substr($file["name"],-3);
			$filename_thumb = str_replace(".".$file_ext , "_thumb".".".$file_ext , $file["name"]);
			if(file_exists($this->upload_dir."/".$filename_thumb)){
				$path = LACNE::get_filepath($this->upload_path,$filename_thumb);
			}
			else
			{
				$path = LACNE::get_filepath($this->upload_path,$file["name"]);
			}
			return $path;
        }
        else if($file["type"] == "movie")
        {
            if(isset($this->movie_thumb_arr[$file["ext"]]))
            {
                return LACNE::get_filepath($this->option_view_path,$this->movie_thumb_arr[$file["ext"]]);
            }
        }
        else if($file["type"] == "pdf")
        {
            if(isset($this->etcfile_thumb_arr["pdf"]))
            {
                return $this->etcfile_thumb_arr["pdf"];
            }
        }
        
        return "";
    }
    
    /**
     * ファイル拡張子の種類からメディアタイプを返す
     * @param string $extension
     * @return string 
     */
    function get_media_type($extension)
    {
        if($extension && ($extension == "jpg" || $extension == "png" || $extension == "gif"))
        {
            return "image";
        }
        else if($extension && ($extension == "flv" || $extension == "wmv" || $extension == "mov"))
        {
            return "movie";
        }
        else if($extension && ($extension == "pdf"))
        {
            return "pdf";
        }
        return "etc";
    }
    
}



?>
<?php namespace lacne\core\model;

use lacne\core\Model;
/**
// ------------------------------------------------------------------------
 * model_social_connect.php
 * ソーシャル連携用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class SocialConnect extends Model
{

    /**
     * SocialConnect constructor.
     */
    public function __construct() { parent::__construct(); }

    function fetchAll()
    {
        $sql = "SELECT * FROM ".TABLE_OPTION_SOCIAL." ORDER BY id DESC";
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".TABLE_OPTION_SOCIAL." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    /**
     * ソーシャルメディアへの接続データ（認証KEYまわり）取得
     *
     * @return object
     */
    function get_infodata()
    {
        $sql = "SELECT * FROM ".TABLE_OPTION_SOCIAL." WHERE type = ? OR type = ?";
        return $this->_fetchAll($sql, array('twitter','facebook'));
    }

    function replace($data , $key)
    {
        return $this->adodb_replace(TABLE_OPTION_SOCIAL , $data , $key);
    }

    function delete($where = "" , $param = array())
    {

        $sql = "DELETE FROM ".TABLE_OPTION_SOCIAL." WHERE ".$where;
        return $this->_execute($sql, $param);

    }

    function cnt($where = "" , $param = array())
    {
        return $this->_cnt(TABLE_OPTION_SOCIAL , $where , $param);
    }

}

?>
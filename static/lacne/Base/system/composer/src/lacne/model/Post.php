<?php namespace lacne\core\model;
use lacne\core\Model;
/**
// ------------------------------------------------------------------------
 * model_post.php
 * 記事データ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Post extends Model
{

    /**
     * Post constructor.
     */
    public function __construct() { parent::__construct(); }

    /**
     * 全データ取得
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POSTS);
        return $this->_fetchAll($sql, array());
    }

    /**
     * 指定したidのデータ1件取得
     * @param number $id
     * @return object
     */
    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT post.* , category.category_name FROM ".$this->getTableName(TABLE_POSTS)." as post
                        LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
                        WHERE post.id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }


    /**
     *
     * データのインサートもしくはアップデート処理
     *
     * @param object $data
     * @param string $key
     * @param boolean $auto_quote
     */
    function replace($data , $key , $auto_quote = true)
    {
        $tid = $this->adodb_replace($this->getTableName(TABLE_POSTS) , $data , $key);

        return $tid;
    }

    function delete($id)
    {
        if(is_numeric($id))
        {
            $sql = "DELETE FROM ".$this->getTableName(TABLE_POSTS)." WHERE id = ?";
            $this->_execute($sql, array($id));
        }

        return;
    }

    /**
     * 該当するデータの家運t
     *
     * @param string $where
     * @param array  $param
     */
    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_POSTS) , $where , $param);
    }

    /**
     * 一覧取得用のSQL生成
     * @param object $search_param
     * @param object $order
     * @return string
     */
    function get_list_sql($search_param, $order = array())
    {

        $sql = "SELECT post.*, category.category_name
                    FROM ".$this->getTableName(TABLE_POSTS)." as post
                    LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id";

        if(is_array($search_param) && count($search_param))
        {
            $cnt = 0;
            foreach($search_param as $key => $val)
            {
                if(!$cnt) $sql .= " WHERE ";
                else $sql .= " AND ";
                $sql .= $key . " ".$this->esc($val);
                $cnt++;
            }
        }

        //ソート調整
        if(isset($order["key"]) && $order["key"]){
            $sql .= " ORDER BY ".$order["key"];
            if(isset($order["by"]) && $order["by"]){
                $sql .= " ".$order["by"];
            }
            else
            {
                $sql .= " DESC ";
            }
            $sql .= ",";
            if($order["val"] != "sort_date")
            {
                $sql .= "post.output_date DESC ,";
            }

            $sql .= "post.id DESC";
        }

        return $sql;
    }

    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list($page , $limit , $search_param = array() , $order = array())
    {
        $sql = $this->get_list_sql($search_param , $order);

        //SELECT news1.* , news2.id as news2_id , news2.status as news2_status FROM news as news1 LEFT JOIN news as news2 ON news1.id = news2.news_base_id WHERE news1.news_base_id = 0

        return $this->_fetchPage($sql, $page, $limit);
        //$sql .= " LIMIT ".$limit." OFFSET ".(($page-1)*$limit);
        //return $this->_fetchAll($sql,array());
    }
    /**
     * 一覧データ取得（1ページあたり$limit件数とさらに$plus件数分余分にデータ取得する）
     * @param number $page
     * @param number $limit
     * @param number $plus
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list_plus($page , $limit , $plus , $search_param = array() , $order = array())
    {
        $sql = $this->get_list_sql($search_param , $order);
        $sql .= " LIMIT ".($limit+$plus)." OFFSET ".(($page-1)*$limit);
        return $this->_fetchAll($sql,array());
    }


    /**
     * 登録されている記事データの総件数
     * @param object $search_param
     * @return number
     */
    function data_cnt($search_param)
    {
        $sql = $this->get_list_sql($search_param);
        if($rs = $this->conn->Execute($sql)){
            return $rs->RecordCount();
        }

        return 0;
    }

    /**
     * カテゴリごとに何件のデータがあるかを取得
     * @param string $where
     * @param array $param
     * @return array
     */
    function getnum_category_by($where = "" , $param = array())
    {

        $result = array();
        $sql = "SELECT category , count(id) as cnt FROM ".$this->getTableName(TABLE_POSTS);
        if($where)
        {
            $sql .= " WHERE ".$where;
        }

        $sql .= " GROUP BY category ORDER BY category ASC";

        $rs = $this->_fetchAll($sql , $param);
        if($rs)
        {
            foreach($rs as $data)
            {
                $result[$data["category"]] = $data["cnt"];
            }
        }

        return $result;
    }

    /**
     * 指定したアカウントIDを持つ記事をすべて任意のアカウントIDに変更する
     * （アカウント管理画面でアカウント削除を行った場合にそのアカウントで記事が作成されていた場合の対処）
     * @param number $target_id
     * @param number $change_id
     * @return number
     */
    function change_user_id($target_id , $change_id = 1)
    {
        $sql = "UPDATE ".$this->getTableName(TABLE_POSTS)." SET user_id = ? WHERE user_id = ?";
        return $this->_execute($sql , array($change_id , $target_id));
    }

    /**
     * POSTデータのカテゴリを一括変更させる（カテゴリ削除処理の際、すべてcategory=1に変更する処理で使う）
     * @param number $target_category 変更対象カテゴリID
     * @param number $change_category 変更後のカテゴリID
     * @return boolean
     */
    function post_category_change($target_category , $change_category)
    {
        $sql = "UPDATE ".$this->getTableName(TABLE_POSTS)." SET category = ? WHERE category = ?";
        return $this->_execute($sql , array($change_category , $target_category));
    }

    /**
     * 指定したアカウントIDで投稿データを絞り込んで取得
     * iphoneアプリ用 (さらにアプリから投稿されたデータのみに絞り込む必要がある※app_modifie値があるデータのみ取得）
     * @param type $user_id
     */
    function get_appdata_list($user_id)
    {
        //app_modifiedがmodifiedよりも過去ならアプリ投稿された内容がPC側で編集された可能性あり
        $sql = "SELECT * , IF(app_modified < modified , 1 , 0) as edit_pc ,
                    CASE
                        WHEN output_flag = 1 THEN 1
                        WHEN status = 'wait' THEN 3
                        WHEN output_flag = 0 THEN 2
                    END as status_val
                    FROM ".$this->getTableName(TABLE_POSTS)." WHERE user_id = ? AND app_modified != '0000-00-00 00:00:00' ORDER BY output_date DESC";

        return $this->_fetchAll($sql , array($user_id));

    }


}

?>
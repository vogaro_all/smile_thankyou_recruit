<?php namespace lacne\core\model;
use lacne\core\Model;

class PostTimetable extends Model
{

    function __construct(){ parent::__construct(); }

    function replace($post_id, $data) {
        $sql = "DELETE FROM post_timetable WHERE post_id = ?";
        $this->_execute($sql, array($post_id));

        $i = 1;
        $created = date('Y-m-d H:i:s');
        foreach ($data as $v) {
            $this->adodb_replace('post_timetable', array(
                'post_id' => $post_id,
                'time' => $v['time'],
                'content' => $v['content'],
                'sort' => $i++,
                'created' => $created
            ), 'id');
        }
    }

    function fetchByPostID($post_id)
    {

        $sql = "SELECT * FROM post_timetable WHERE post_id = ? ORDER BY sort ASC";
        return $this->_fetchAll($sql, array($post_id));

    }
}

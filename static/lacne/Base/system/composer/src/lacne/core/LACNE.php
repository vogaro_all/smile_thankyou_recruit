<?php
use lacne\core\AdoDB;
use lacne\core\Template;
use lacne\core\Request;
use lacne\core\Response;
use lacne\core\Session;

/**
 * Class
 */
class LACNE
{
    var $db;
    var $session;
    var $template;
    var $response;
    var $option;
    var $url_setting;
    var $request;

    var $action_name;
    var $url_parameters;
    var $model;
    var $library;

    function __construct(){

        $this->option = new Option_setting();
        $this->url_setting = new PageUrl_setting();

        $this->db       = new AdoDB();
        $this->session  = new Session();
        $this->template = new Template(LACNE_TEMPLATE_PATH);
        $this->response = new Response();
        $this->request  = new Request();

        //$this->rhaco_setup();
        $this->url_routing();
    }

    function rhaco_setup()
    {
        //include_once(DIR_VENDORS."rhaco/Rhaco.php");
        /*
        Rhaco::import("exception.ExceptionTrigger");
        Rhaco::import("exception.model.NotFoundException");
        */
    }

    function url_routing()
    {
        $this->action_name = '/';
        if(isset($_GET["action"]) && $_GET["action"])
        {
            $this->action_name .= fn_esc($_GET["action"]);
        }
    }


    /**URLルーティングで抽出したアクション名と引数値を照合する処理
     *
     * @param string 照合するaction_name 一致すればTRUE
     */
    function action($action)
    {
        if($action === $this->action_name)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function post($action , $match = "")
    {
        return $this->request->post_action($action , $match);
    }

    /**
     * projectフォルダへのフルパスを返す（/var/www/htdocs/lacne）
     * @return string
     */
    function getLACNEpath()
    {
        return LACNE_DIR;
    }

    /**
     * LACNE設置パスと渡されたpathとの差分となるパスを返す
     * @param string $path
     * @return string
     */
    function get_diff_path($path)
    {
        return str_replace("\\","/",str_replace(LACNE::getLACNEpath(), "", $path));
    }

    /**
     * 渡されたフルパスを絶対パス(document rootからの)にして返す
     * @param string $path
     * @return string
     */
    function get_path_docroot($path)
    {
        return LACNE_PATH.LACNE::get_diff_path($path);
    }

    /**
     * 渡されたパス(document rootからの絶対パス)をサーバールートからのフルパスにして返す
     * @param string $path
     * @return string
     */
    function pathChange_dir_root($path)
    {
        return LACNE_DIR.(str_replace(LACNE_PATH , "" , $path));
    }

    /**
     * $base（指定したディレクトリ）から%pathへの相対パスを取得する
     * @param string $base
     * @param string $path
     * @return string
     */
    function get_filepath($base,$path=""){
        $base = str_replace("\\","/",$base);
        if(substr($base,-1) != "/" && substr($path,0,1) != "/") $base .= "/";
        if($path == "") return $base;
        $path = str_replace("\\","/",$path);
        return $base.$path;
    }

    /**
     * ページからのパス（docrootからの絶対パス形式）を生成
     * 主にtemplate中の画像やjsなどのパス指定に使う
     * @param string $path ( /a/b -> LACNE_PATH."/a/b" , ../a/ -> ページからの相対パス指定  ,  $pathなし -> LACNE設置位置からページ自身へのパス)
     * @return type
     */
    function app_path($path = "")
    {
        if(substr($path,0 , 1) == ".")
        {
            return LACNE::get_path_docroot(realpath(getcwd()."/".$path));
        }
        else if(!empty($path))
        {
            return LACNE::get_path_docroot(getcwd()."/".$path);
        }
        else
        {
            return LACNE::get_path_docroot(getcwd());
        }
    }

    /**
     * 自身のURL情報（http(s)://server_name ホスト名）を返す
     */
    function get_url_hostname()
    {
        $url = "";
        if ( (false === empty($_SERVER['HTTPS']))&&('off' !== $_SERVER['HTTPS']) ) {
            $url .= "https://";
        }
        else
        {
            $url .= "http://";
        }

        return $url .= $_SERVER["SERVER_NAME"];
    }

    /**
     * 自身のURL情報を返す
     *
     * @return string
     */
    function get_self_url()
    {
        return LACNE::get_url_hostname().$_SERVER["SCRIPT_NAME"];
    }

    /**
     * httpからのURLをルートとしたパスを返す
     * @param string $file_path
     */
    function get_urlpath($file_path = "")
    {
        return LACNE::get_url_hostname().((substr($file_path,0,1) == "/") ? $file_path : "/".$file_path);
    }


    /**
     * modelファイルのロード
     * @param array $model
     * @return void
     */
    function load_model($model) {

        if(is_array($model))
        {

            foreach($model as $m)
            {
                $model_name = "Model_".$m;

                //まずローカル側のMODELを確認
                $load_file = LACNE_APP_DIR_MODEL.$model_name.".php";
                if(!file_exists($load_file))
                {
                    //ローカルになければコア側のファイルを利用
                    $load_file = DIR_MODEL.$model_name.".php";
                }

                if(file_exists($load_file) && !isset($this->model[$m]))
                {
                    if(empty($this->db->conn)){
                        $this->db->connect();
                    }

                    include_once($load_file);
                    $this->model[$m] = new $model_name($this , $this->db);

                    //読み込んだモデルを拡張させるオプションの指定があれば
                    //そのオプションを読み込み、インスタンス生成
                    $option_model = $this->option->get_option_model($m);

                    if($option_model && isset($option_model["src"]) && isset($option_model["class_name"]))
                    {
                        //オプションをロードして、インスタンス化
                        $opt_m_obj = $this->load_option($option_model["src"] , $option_model["class_name"]);
                        if($opt_m_obj)
                        {
                            $this->model[$m] = $opt_m_obj;
                        }
                        /*
                        if(file_exists(DIR_OPTION.$option_model["src"]))
                        {
                            include_once(DIR_OPTION.$option_model["src"]);
                            $this->model[$m] = new $option_model["class_name"]($this , $this->db);
                        }
                         */
                    }
                }
            }
        }

        return;
    }

    /**
     * libraryファイルのロード
     * @param array $library
     * @return void
     */
    function load_library($library)
    {

        if(is_array($library))
        {
            foreach($library as $l)
            {
                $library_name = "Lib_".$l;

                //まずローカル側のLIBRARYを確認
                $load_file = LACNE_APP_DIR_LIBRARY.$library_name.".php";
                if(!file_exists($load_file))
                {
                    //ローカルになければコア側のファイルを利用
                    $load_file = DIR_LIBRARY.$library_name.".php";
                }
                if(file_exists($load_file) && !isset($this->library[$l]))
                {
                    if(empty($this->db->conn)) $this->db->connect();

                    include_once($load_file);
                    $this->library[$l] = new $library_name($this , $this->db);

                    //読み込んだライブラリを拡張させるオプションの指定があれば
                    //そのオプションを読み込み、インスタンス生成
                    $option_library = $this->option->get_option_library($l);

                    if($option_library && isset($option_library["src"]) && isset($option_library["class_name"]))
                    {
                        //オプションをロードして、インスタンス化
                        $opt_l_obj = $this->load_option($option_library["src"] , $option_library["class_name"]);
                        if($opt_l_obj)
                        {
                            $this->library[$l] = $opt_l_obj;
                        }
                        /*
                        if(file_exists(DIR_OPTION.$option_library["src"]))
                        {
                            include_once(DIR_OPTION.$option_library["src"]);
                            $this->library[$l] = new $option_library["class_name"]($this , $this->db);
                        }
                         */
                    }
                }
            }
        }

        return;
    }


    /**
     * オプションファイルの読み込み
     * @param string $option_file
     * @param string $class_name
     * @return mixed
     */
    function load_option($option_file , $class_name)
    {

        //ローカル側のOPTIONファイルを確認
        $load_file = LACNE_APP_DIR_OPTION.$option_file;
        if(!file_exists($load_file))
        {
            //ローカルになければコア側のファイルを利用
            $load_file = DIR_OPTION.$option_file;
        }
        if(file_exists($load_file))
        {
            include_once($load_file);

            return new $class_name($this , $this->db);
        }
        return false;
    }

    /**
     * テンプレートに変数を埋め込み表示させる
     * @param string $tpl
     * @param object $vars
     * @param boolean $return //出力内容をそのまま出力せず、返却させる場合はtrueを指定
     * @return mixed
     */
    function render($tpl , $vars , $return = false)
    {

        $this->template->setVars(array(
            "LACNE" => $this,
            "APP_PATH" => $this->get_path_docroot(getcwd())
        ));

        $content = $this->template->render($tpl , $vars , $return);
        if($return)
        {
            return $content;
        }

        $this->response->output($content);
    }
}


/*---------------------------------
 *  magic_quotes_gpc = On の場合の対策
 *--------------------------------*/
if(!function_exists('strip_magic_quotes_slashes')) {
    if (get_magic_quotes_gpc()) {
        function strip_magic_quotes_slashes($arr)
        {
            return is_array($arr) ?
                array_map('strip_magic_quotes_slashes', $arr) :
                stripslashes($arr);
        }

        $_GET     = strip_magic_quotes_slashes($_GET);
        $_POST    = strip_magic_quotes_slashes($_POST);
        $_REQUEST = strip_magic_quotes_slashes($_REQUEST);
        $_COOKIE  = strip_magic_quotes_slashes($_COOKIE);
    }
}

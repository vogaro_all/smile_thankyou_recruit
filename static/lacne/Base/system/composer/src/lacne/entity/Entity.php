<?php
namespace lacne\entity;

abstract class Entity {
    
    /**
     * Entity constructor.
     * @param $properties
     */
    public function __construct($properties) {
        $properties = (array) $properties;
        foreach($properties as $prop => $value){
            $this->__set($prop, $value);
        }
    }

    /**
     * ゲッタ
     * @param $prop
     * @return mixed|void
     */
    public function __get($prop)
    {
        if (method_exists($this, $prop)) {
            return $this->$prop();
        }

        if (property_exists($this, $prop)) {
            return $this->$prop;
        }

        return;
    }

    /**
     * セッタ
     * @param $prop
     * @param $value
     */
    public function __set($prop, $value)
    {
        $prop = $this->camelize($prop);
        $this->$prop = $value;
        return;
    }

    /**
     * プロパティの存在チェック
     * @param $prop
     * @return boolean
     */
    public function __isset($prop)
    {
        if (property_exists($this, $prop)) {
            return true;
        }

        if (method_exists($this, $prop)) {
            return true;
        }

        return false;
    }

    /**
     * @param $str
     * @return string
     */
    protected function camelize($str)
    {
        $camelizedStr = lcfirst(strtr(ucwords(strtr($str, array('_' => ' '))), array(' ' => '')));
        if (strpos($str, '_') === 0) {
            return '_' . $camelizedStr;
        }
        return $camelizedStr;
    }
}
<?php
namespace lacne\entity;

class PostSpecEntity extends Entity {

    protected $limit;
    protected $page;
    protected $category;

    /**
     * PostEntity constructor.
     * @param array $properties
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);
    }

    public function setPage($page) {
        $this->page = fn_now_page_set($page);
    }

    public function setCategory($category){
        $this->category = in_array($category, array(1,2)) ? $category : null;
    }

    /**
     * セッタ
     * @param $prop
     * @param $value
     */
    public function __set($prop, $value)
    {
        $setter = $this->camelize('set_' . $prop);
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $prop = $this->camelize($prop);
            $this->$prop = $value;
        }
        return;
    }
}
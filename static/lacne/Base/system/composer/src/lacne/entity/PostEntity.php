<?php
namespace lacne\entity;

class PostEntity extends Entity {

    protected $id;
    protected $outputDate;
    protected $category;
    protected $categoryName;
    protected $link;
    protected $linkWindow;
    protected $detailFlag;
    protected $body;

    /**
     * PostEntity constructor.
     * @param array $properties
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);
    }

    public function outputDate($fmt='Y.m.d') {
        return fn_dateFormat($this->outputDate, $fmt);
    }

    public function detailLinkUrl() {
        return $this->link ? $this->link : '/news/detail?id=' . $this->id;
    }

    public function detailLinkTarget(){
        return $this->link && $this->link_window ? '_blank' : '_self';
    }

    public function ifNew($str){
        return (strtotime($this->outputDate) >= strtotime('-7 day')) ? $str : '';
    }

    public function body() {
        return preg_replace_callback("/<p class=\"blocklink\"><a (.*?)>(.*?)<\/a><\/p>/", function($matches) {
            $attrs = array();
            foreach (explode(' ', $matches[1]) as $attr) {
                $attr = trim($attr);
                list($prop, $value) = explode('=', $attr);
                if (in_array($prop, array('href', 'target'))) {
                    $attrs[] = $attr;
                }
            }

            return '
                <a ' . implode(' ', $attrs) . ' class="sentence-link">
                    <span class="sentence-link__arrow">
                        <svg class="icon icon--arrow" width="15" height="9"><use xlink:href="#svg-arrow-icon"></use></svg>
                    </span>' . $matches[2]. '
                    <span class="link-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill-rule="evenodd" fill="#373E42" d="M8 6.999V1H2V-.001h7v7H8zM7 2v7H0V2h7zM1 8h5V3H1v5z"/></svg>
                    </span>
                </a>
            ';
        }, $this->body);
    }
}
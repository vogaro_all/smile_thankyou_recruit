<?php
namespace lacne\service;

use lacne\entity\PostSpecEntity as Spec;

class Pager {

    protected $total;
    protected $limit;
    protected $currentPage;
    protected $firstPage;
    protected $lastPage;
    
    /**
     * Pager constructor.
     * @param $total
     * @param $limit
     * @param $page
     */
    public function __construct($total, $limit, $page)
    {
        $this->total = $total;
        $this->limit = $limit;

        $this->firstPage = 1;
        $this->lastPage = ceil($this->total / $this->limit);

        $this->currentPage = $page;
    }

    public function pageList($maxShowNum = null) {

        if ($maxShowNum) {
            $center = ceil($maxShowNum / 2);
            if ($this->currentPage <= $center) {
                $start = 1;
                $end = $maxShowNum;
            } elseif($this->currentPage > $this->lastPage - $center) {
                $start = $this->lastPage - ($maxShowNum - 1);
                $end = $this->lastPage;
            } else {
                $start = $this->currentPage - ($center - 1);
                $end = $this->currentPage + ($maxShowNum - $center);
            }
            if ($start < 1) {
                $start = 1;
            }
            if($end > $this->lastPage) {
                $end = $this->lastPage;
            }
        } else {
            $start = $this->firstPage;
            $end = $this->lastPage;
        }

        $list = array();
        for($i = $start; $i <= $end; $i++) {
            $list[] = $i;
        }

        return $list;
    }

    public function pageLink($page){
        $params = $_GET;
        $params['page'] = $page;
        return '/news/?' . http_build_query($params);
    }

    public function prevPageLink() {
        return $this->existsPrevPage()
            ? $this->pageLink($this->currentPage - 1)
            : $this->pageLink($this->firstPage);
    }

    public function nextPageLink() {
        return $this->existsNextPage()
            ? $this->pageLink($this->currentPage + 1)
            : $this->pageLink($this->lastPage);
    }

    public function isPageSplitted() {
        return $this->lastPage > 1;
    }

    public function isCurrentPage($page) {
        return $page == $this->currentPage;
    }

    public function existsPrevPage() {
        return $this->currentPage > $this->firstPage;
    }

    public function existsNextPage() {
        return $this->currentPage < $this->lastPage;
    }

    /**
     * ゲッタ
     * @param $prop
     * @return mixed|void
     */
    public function __get($prop)
    {
        if (method_exists($this, $prop)) {
            return $this->$prop();
        }

        if (property_exists($this, $prop)) {
            return $this->$prop;
        }

        return;
    }

    /**
     * プロパティの存在チェック
     * @param $prop
     * @return boolean
     */
    public function __isset($prop)
    {
        if (property_exists($this, $prop)) {
            return true;
        }

        if (method_exists($this, $prop)) {
            return true;
        }

        return false;
    }
}
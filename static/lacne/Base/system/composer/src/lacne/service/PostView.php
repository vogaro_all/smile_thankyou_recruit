<?php
namespace lacne\service;

use lacne\core\model\Category;
use lacne\entity\PostEntity;
use lacne\service\Pager;
use lacne\entity\PostSpecEntity as Spec;

class PostView {

    const PAGE_LIMIT = 2;
    const PAGER_SHOW_NUM = 10;

    private $lacne;

    public function __construct() {
        global $Output;
        $this->lacne = $Output;
    }


    public function getPosts(Spec $spec) {
        $params = array();
        if ($spec->category) {
            $params['category'] = $spec->category;
        }
        if ($spec->limit) {
            $params['page_limit'] = $spec->limit;
            $params['pager'] = true;
        }
        $data = $this->lacne->get_post_list($params , array(), false);
        if (!$data || empty($data)) {
            return array();
        }
        $posts = array();
        foreach ($data as $v) {
            $posts[] = new PostEntity($v);
        }
        return $posts;
    }

    public function getPager(Spec $spec) {
        $params = array();
        if ($spec->category) {
            $params['category'] = $spec->category;
        }
        $cnt = $this->lacne->get_post_list($params , array(), true);

        return new Pager($cnt, $spec->limit, $spec->page);
    }

    public function getPost($id = '') {
        $data = LACNE_Post($id);
        if (!$data) {
            throw new \RuntimeException('Not Found', 404);
        }

        //管理画面の動画編集画面からのプレビューの場合
        if ($this->isEditingPreviewMode()) {
            //カテゴリ名補完
            $category = with(new Category())->fetchOne($data['category']);
            $data['categoryName'] = $category ? $category['category_name'] : '';
        }

        $entity = new PostEntity($data);

        return $entity;
    }

    private function isEditingPreviewMode(){
        return $this->lacne->preview_mode && isset($_POST);
    }
}
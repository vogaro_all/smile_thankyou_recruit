<?php
use lacne\core\model\Post;
use lacne\core\model\PostTimetable;
return function($request, $response, $service, $app) {

    $app->lacne->load_library(array('login', 'post', 'media', 'validation')); //library load
    $app->lacne->session->sessionStart(); //Session start
    $login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにarticle以下にあるため）
    $app->lacne->template->setViewDir($app->lacne->template->getViewDir() . "/article");
    //render_data : Template側に渡すデータ変数
    $render_data = array(
        "login_id" => $login_id
    );

    //記事の作成・編集が可能かどうかをチェック
    $post_authority = 1;
    //編集モードの場合、公開権限があるか、またはまだ公開されていない自身作成の記事であるかをチェック
    if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
        $rs_data = with(new Post())->fetchOne($_GET["id"]);
        if (empty($rs_data)) $post_authority = 0;
        if (!($app->lacne->library["login"]->chk_controll_limit("publish_post") || (!$rs_data["output_flag"] && isset($rs_data["user_id"]) && $login_id == $rs_data["user_id"]))) {
            $post_authority = 0; //編集不可
        }
    }

    // ------------------------------------------------------------------------
    // ここからPOST 登録処理
    // ------------------------------------------------------------------------
    if ($app->lacne->action('/submit')) {

        //直接アクセスはリダイレクト
        if (!isset($_POST["title"])) {
            fn_redirect(LACNE_APP_ADMIN_PATH . "/article/edit.php");
        }

        //送信されてきたパラメータ取得
        $data_list = fn_get_form_param($_POST);

        if (isset($data_list["_meta_"]) && $data_list["_meta_"]) {
            $_meta_data = $data_list["_meta_"];
        }

        //入力チェック
        $err_check_arr = array(
            "title" => array("name" => "タイトル", "type" => array("null", "len"), "length" => 200),
            "output_date" => array("name" => "公開日時", "type" => array("null", "len", "output_date"), "length" => 19),
            "category" => array("name" => "カテゴリ", "type" => array("null", "numeric")),
            "link"        => array("name" => "リンク先", "type" => array("len"), "length" => 255),
        );
        //バリデーション実行
        $err = $app->lacne->library["validation"]->check($data_list, $err_check_arr);


        //編集モードなら
        if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
            $rs_data = with(new Post())->fetchOne($_GET["id"]);

            //編集モードの場合、公開権限があるか、またはまだ公開されていない自身作成の記事であるかをチェック
            if ($post_authority) {
                //最終更新日時をチェックし、間に別アカウントで同じ記事を編集されていないか
                //チェック（複数アカウント対応用）
                if ($rs_data && $rs_data["modified"] != $data_list["modified"]) {
                    $err["update_err"] = "別のユーザーによってこの" . KEYWORD_KIJI . "の編集が行われた可能性があるため、この編集内容を保存することができません。<br />" . KEYWORD_KIJI . "一覧画面からもう一度編集作業をお試しください。";
                }
            } else {
                $err["update_err"] = "公開権限がないため、この" . KEYWORD_KIJI . "を編集することができない状態になっていない可能性があります。";
            }

            if (!$err) {
                $data_list["id"] = $_GET["id"];
                $data_list["modified"] = fn_get_date();
            }

        } else {
            //新規登録モードなら
            $data_list["sort_no"] = 0;
            $data_list["output_flag"] = 0;
            $data_list["created"] = fn_get_date();
            $data_list["modified"] = fn_get_date();
        }

        //CSRF TOKENチェック
        if (!$err) {
            $csrf_check = false;
            if (isset($data_list["token"]) && $data_list["token"]) {
                $csrf_check = $app->lacne->request->csrf_check($data_list["token"]);
            }
            if (!$csrf_check) $err["csrf_check"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
        }

        //エラーがあれば、エラー出力 なければ確認画面へ
        if (!$err) {

            //記事作成（編集）者
            $data_list["user_id"] = $login_id;
            //unset data
            if (isset($data_list["_meta_"])) unset($data_list["_meta_"]);
            if (isset($data_list["token"])) unset($data_list["token"]);


            //詳細内容あり / なし
            //なしにチェックが入っていれば詳細内容をクリアする
            if (isset($data_list["detail_on"]) && !$data_list["detail_on"]) {
                $data_list["body"] = "";
            }

            unset($data_list["detail_on"]);

            //本文のbodyデータでscript関連タグを除去
            if (isset($data_list["body"]) && $data_list["body"]) {
                $data_list["body"] = fn_strip_jscode($data_list["body"]);
            }

            //リンク先ウインドウで値がなければ0指定で登録する
            if (isset($data_list["link_window"]) && $data_list["link_window"]) {
                $data_list["link_window"] = 1;
            } else {
                $data_list["link_window"] = 0;
            }

            //DB登録
            $post_id = $app->lacne->library['post']->replace_post($data_list, "id");


            //=============================
            //カスタムフィールド内容を登録
            //=============================
            if (isset($post_id) && $post_id && isset($_meta_data) && $_meta_data) {
                $meta_rs = $app->lacne->library['post']->replace_meta($post_id, $_meta_data);
                $data_list["_meta_"] = $_meta_data;
            }

            if ($post_id) {
                //登録完了
                //リダイレクトさせて、完了メッセージを表示させる
                //complete : 登録完了 ,  completewait : 承認待ち状態で登録
                //まず登録した記事が承認待ちかどうかチェック
                $post_info = with(new Post())->fetchOne($post_id);
                $message_type = "complete";
                if ($post_info["status"] == "wait") {
                    $message_type = "completewait";
                }
                fn_redirect(LACNE_APP_ADMIN_PATH . "/article/edit.php?id=" . $post_id . "&" . $message_type);
            } else {
                //返却された$post_idが0 or falseならエラー発生の可能性
                $render_data["err"] = "データ登録時に問題が発生した可能性があります";
            }

        } else {
            //エラー
            $render_data["err"] = $err;
        }
    }


    //編集モードの場合
    if (!isset($data_list) && isset($_GET["id"]) && fn_check_int($_GET["id"])) {

        $data_list = $app->lacne->library['post']->get_postdata($_GET["id"]);

        //表示日時をY-m-d H:i形式に補正
        if (isset($data_list["output_date"]) && $data_list["output_date"]) {
            $data_list["output_date"] = date("Y-m-d H:i", strtotime($data_list["output_date"]));
        }
        //登録完了後にリダイレクトしてきた場合、表示メッセージを用意する
        if (isset($_GET["complete"])) {
            $render_data["message"] = KEYWORD_KIJI . "の登録・編集が完了しました。<br />新規作成された場合、" . KEYWORD_KIJI . "一覧画面において公開切り替えの操作を行う必要があります。";
        } else if (isset($_GET["completewait"])) {
            $render_data["message"] = KEYWORD_KIJI . "の登録・編集が完了しました。<br />この" . KEYWORD_KIJI . "は承認待ちの状態として保存されました。管理者による承認が行われるまで公開されません。";
        }
    }

    $render_data = array_merge($render_data, array(
        "post_authority" => $post_authority,
        "data_list" => $data_list,
        "category_list" => $app->lacne->library['post']->get_category_list(), //登録されているカテゴリデータを取得
        "csrf_token" => $app->lacne->request->csrf_token_generate()
    ));

    //Render
    return $app->lacne->render("edit", $render_data, true);

};
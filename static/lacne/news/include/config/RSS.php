<?php

/**
 * ローカル用の設定ファイル、拡張するクラス等をロード
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */

/*----------------------------------------------
 *  RSS
 *  [_CHECK_] TITLE、SUBTITLEあたりを適宜編集
 *---------------------------------------------*/
define("LACNE_RSS_ENABLED" , false); //RSSを有効にするか
define("LACNE_RSS_SITEURL" , SITE_URL); //RSSに掲載するサイトURL
define("LACNE_RSS_FILENAME" , "feed.php");
define("LACNE_RSS_URL" , LACNE_APP_URL."/rss/".RSS_FILENAME);
define("LACNE_RSS_TITLE" , "新着ニュースRSS");
define("LACNE_RSS_SUBTITLE" , ""); //サブタイトル,説明テキストなど
define("LACNE_RSS_OUTPUT_NUM" , 20); //書き出し件数


?>

<?php

    $page_setting = array(
        "title" => "ファイル一覧",
        "js" => array(
            LACNE_SHAREDATA_PATH."/js/medialist.js",
            LACNE_SHAREDATA_PATH."/js/mediadrop.js",
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip-1.1.0.min.js",
        ),
        "css" => array(
            LACNE_SHAREDATA_PATH."/css/media/list.css",
            LACNE_SHAREDATA_PATH."/css/media/drop_upload.css",
            LACNE_SHAREDATA_PATH."/css/common/pager.css",
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip.css"
        )
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    $('.tip').powerTip({
        placement:'s',
        fadeInTime:100
    });
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "list");
?>


<section class="section">
	
<ul class="tab">
<li class="css3 on"><a href="list.php?<?=$search_param?>" class="css3">ファイル一覧</a></li>
<li class="css3"><a href="list.php?upload=on&<?=$search_param?>" class="css3">アップロード</a></li>
<!-- .tab // --></ul>	

<?php
if(!empty($media_data) || (empty($media_data) && !empty($search_tag))) :
?>
<p class="section-inside">※挿入するメディアファイルを選択して下さい。</p>

<form action="" method="get" id="tagSearchForm">
<div>
<div class="mt10"><label class="labelstr">タグ検索：</label><input type="text" name="search_tag" value="<?=!empty($search_tag)?$search_tag:''?>" maxlength="100" size="15" /><input type="submit" class="btn-tagsearch" value="検索" />
<?php if(!empty($_GET["search_tag"])) : ?>
<input type="button" class="btn-tagsearch" id="btnAllView" value="すべて表示" />
<?php endif; ?>
<?php if(!empty($taglist_data)) : ?>
<p class="taglist">
<?php foreach($taglist_data as $tag) : ?>
<span><a href="#" class="tag"><?=$tag["name"]?></a> (<?=$tag["cnt"]?>)</span>
<?php endforeach; ?>
</p>
<?php endif; ?>
</div>
</div>

<?php if(!empty($_GET["movie"])) { ?>
<input type="hidden" name="movie" value="1" />
<?php } ?>
<?php if(!empty($type)) { ?>
<input type="hidden" name="type" value="<?=$type?>" />
<?php } ?>
<?php if(!empty($field_name)) { ?>
<input type="hidden" name="field_name" value="<?=$field_name?>" />
<?php } ?>

</form>

<ul class="list-media">
<?php
    if(is_array($media_data)) :
        foreach($media_data as $media) :
?>
<li>    
<?php
if($type == "meta") :
?>
<p class="img" onclick="setImageURL_META('<?=$LACNE->library["media"]->get_filepath($media)?>' , '<?=$field_name?>');"><?=$LACNE->library["media"]->
    set_thumbnail($media , $LACNE->library["media"]->get_thumb_height() , $media["tag"] , !empty($media["tag"])?"tip":"")?></p>
<?php
else:
    //$src_target_id = "srcimg";
    $src_target_id = "image";
    if(isset($type_movie) && $type_movie == 1) $src_target_id = "movie";
?>
<p class="img" onclick="setImageURL('<?=$LACNE->library["media"]->get_filepath($media)?>' , '<?=$src_target_id?>');"><?=$LACNE->library["media"]->
    set_thumbnail($media , $LACNE->library["media"]->get_thumb_height() , $media["tag"] , !empty($media["tag"])?"tip":"")?></p>
<?php
endif;
?>
<p class="filename"><?=$media["name"]?></p>
</li>
<?php
        endforeach;
    endif;
?>
</ul>

<div class="pager-foot">
<div class="pager">
<?=fn_pagenavi($page , $page_num , $search_param)?>
<!-- .pager // --></div>
<!-- .pager-info // --></div>

<?php
else:
?>
<p class="section-inside">アップロードされたファイルはまだありません。<br /><br /></p>
<?php
endif;
?>

<div class="btn btn-one">
<p class="btn-type02 pie close_btn" id="btn_close"><a href="#"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>
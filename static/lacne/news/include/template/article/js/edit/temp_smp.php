<script type="text/javascript">
<?php
if($device == "smp") :
// -----------------------------------------------
// smpの場合のみロード
// -----------------------------------------------
?>

var CK_toolbar_smp = [
    ['Source','-','Bold','Italic','Underline','RemoveFormat','-','Link','Unlink'],'/',
    ['Format','FontSize','TextColor']
];
var CK_toolbar_movie_smp = [
    ['Source','-','Bold','Italic','Underline','RemoveFormat','-','Link','Unlink'],'/',
    ['Format','FontSize','TextColor']
];    

$(function(){
    
    Setting_toolbar = CK_toolbar_smp;
    if(INSERT_MOVIE) Setting_toolbar = CK_toolbar_movie_smp; //動画パネル
    CK_EDITOR_HEIGHT = 200; //エディタの高さ指定     
    LACNE_WIDTH_DIALOG = 300; //ファイル選択ダイアログの幅指定    
    CK_EDITOR_WIDTH = 295; //エディタのデフォルト幅指定 
    
    
});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){



});
// -----------------------------------------------
</script>

<?php

    $page_setting = array(
        "title" => "完了",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/complete.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "complete" , array("cancel_page"=>$_cancel_page));
?>

<?php
$status_str = "公開";
if($data["status"] == "back")
{
    $status_str = "差戻し";
}
else if($data["output_flag"] != 1)
{
    $status_str = "非公開に";
}
?>
<section class="section">
<div class="section-inside">
<div class="alert comp pie"><span class="icon">完了</span><p class="fl"><?=KEYWORD_KIJI?>が<?=$status_str?>されました。</p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="<?=$_cancel_page?>"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<!-- .section-inside // --></div>
<!-- .section // --></section>



<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>
<?php

$page_setting = array(
	"title" => KEYWORD_KIJI."作成",
	"js" => array(
		LACNE_SHAREDATA_PATH."/js/jquery-ui-1.8.13.custom.min.js" ,
		LACNE_SHAREDATA_PATH."/js/ckeditor/ckeditor.js",
		LACNE_SHAREDATA_PATH."/js/timepicker/timepicker.js",
		LACNE_SHAREDATA_PATH."/js/medialist.js"
	),
	"css" => array(
		LACNE_SHAREDATA_PATH."/css/article/edit.css",
		LACNE_SHAREDATA_PATH."/js/smoothness/jquery-ui-1.8.13.custom.css",
		LACNE_SHAREDATA_PATH."/js/timepicker/style.css"
	)
);

//include common header template
include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");

?>

	<script type="text/javascript">
		$().ready(function() {
			//Navigation
			$.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .edit');
		});


		var Setting_toolbar = [];
		var LACNE_WIDTH_DIALOG = 450;
		var INSERT_MOVIE = 0;
		<?php if(isset($movie_panel) && $movie_panel) : ?>
		INSERT_MOVIE = 1;
		<?php endif; ?>
	</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "edit" , array("err"=>$err , "message"=>$message , "data_list"=>$data_list));
?>

	<section class="section">
		<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a><?=KEYWORD_KIJI?>作成</h1>
		<p class="load"><?=KEYWORD_KIJI?>の作成、編集を行います。</p>
		<?php
		//--------------------------------------------------------
		// エラー or メッセージ表示
		//--------------------------------------------------------
		?>
		<?php if(isset($err) && $err) : ?>
			<div class="alert error pie" id="comp_message" style="display:none"><span class="icon">エラー</span><p class="fl"><?=fn_output_errtxt($_err)?></p></div>
		<?php elseif(isset($message) && $message) : ?>
			<div id="comp_message" style="display:none">
				<div class="alert comp pie"><span class="icon">完了</span><p class="fl"><?=$_message?></p></div>
				<div class="btn btn-backlist">
					<p class="btn-type01 pie"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php"><span class="pie"><?=KEYWORD_KIJI?>一覧へ</span></a></p>
				</div>
			</div>
		<?php endif; ?>
		<?php if(!$post_authority): ?>
			<div class="alert note pie" id="comp_message"><span class="icon">エラー</span><p class="fl">権限がないか、または公開済みのため、編集を行うことができません。</p></div>
		<?php endif; ?>
		<!-- .section // --></section>

	<section class="section">
		<form action="" method="POST">
			<h2 class="head-line02">動画情報</h2>
			<dl>
				<?php
				//--------------------------------------------------------
				// タイトル
				//--------------------------------------------------------
				?>
				<dt>タイトル：<span class="req">必須</span></dt>
				<dd><input type="text" class="text" name="title" maxlength="200" value="<?=(isset($data_list["title"]))?$data_list["title"]:""?>" /></dd>

				<?php
				//--------------------------------------------------------
				// 公開日時
				//--------------------------------------------------------
				?>
				<dt>公開日時：<span class="req">必須</span></dt>
				<dd>
					<input type="text" name="output_date" id="datetime" class="date" maxlength="19" value="<?=(isset($data_list["output_date"]))?$data_list["output_date"]:""?>" />
					<p class="ex">
						※YYYY-mm-dd HH:ii 形式、もしくはYYYY/mm/dd HH:ii 形式で入力して下さい。<br />
						（例：<?= date('Y-m-d H:i') ?>)<br />
						※時間は省略できます(0:00にセットされます）
					</p>
				</dd>

				<?php
				//--------------------------------------------------------
				// カテゴリ選択
				//--------------------------------------------------------
				?>
				<dt>カテゴリ：<span class="req">必須</span></dt>
				<dd><select name="category"><?=fn_output_html_select($category_list,$data_list["category"])?></select></dd>


				<?php
				//--------------------------------------------------------
				// リンク先
				//--------------------------------------------------------
				?>
				<dt class="cat-change cat1 cat2">リンク先：</dt>
				<dd class="cat-change cat1 cat2"><input type="text" class="text" name="link" maxlength="255" value="<?=(isset($data_list["link"]))?$data_list["link"]:""?>" />
					<p class="ex">
						<label><input type="checkbox" name="link_window" value="1" <?php if(isset($data_list["link_window"]) && $data_list["link_window"] == 1) echo "checked=\"checked\""; ?> /> リンク先ウインドウ</label>（※チェックを入れると別ウインドウで開きます）
					</p>
				</dd>


				<?php
				//--------------------------------------------------------
				// 内容詳細
				//--------------------------------------------------------
				?>
				<dt class="detail-content">内容詳細：</dt>
				<dd class="detail-content">
					<textarea name="body" class="textarea" id="edit-p"><?=(isset($data_list["body"]))?$data_list["body"]:""?></textarea>
					<div class="reset">
						<p class="btn-type03"><strong><a href="#" id="clear" class="pie">詳細クリア</a></strong></p>
					</div>
				</dd>

			</dl>

			<div class="alert note pie pc"><span class="icon">注意</span><p class="fl">編集画面での表示とプレビュー画面での表示は異なる場合があります。</p></div>
			<div class="btn">
				<?php if($post_authority): ?>
					<p class="btn-type01 pie"><span class="pie" id="submit">登録</span></p>
				<?php endif; ?>
				<p class="btn-type02 pie pc"><span class="pie" id="preview">プレビュー</span></p>
				<p class="btn-type02 pie"><span class="pie" id="cancel">キャンセル</span></p>
			</div>
			<input type="hidden" name="modified" value="<?=(isset($data_list["modified"]))?$data_list["modified"]:""?>" />
			<input type="hidden" name="token" value="<?=$csrf_token?>" />
		</form>
		<!-- .section // --></section>


	<section id="Modal" class="section">
		<?php
		//--------------------------------------------------------
		//モーダル画面用
		//--------------------------------------------------------
		?>
		<?=
		//詳細本文をクリア
		//------------------------------
		$LACNE->library["admin_view"]->html_modal_open("box-clear");
		?>
		<div class="alert note pie"><span class="icon">注意</span><p class="fl"><?=KEYWORD_KIJI?>詳細の内容をクリアします。よろしいですか？</p></div>
		<div class="btn">
			<p class="btn-type01 pie"><a href="#" id="delete_link"><span class="pie">クリア</span></a></p>
			<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
			<!-- .btn // --></div>
		<?=$LACNE->library["admin_view"]->html_modal_close();?>

		<!-- #Modal // --></section>

<?php
//include common header template
include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>
<?php
use lacne\core\model\SocialConnect;
/**------------------------------------------------------------------------
 *  
 *  Twitter認証
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login' , 'setting')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

//Social連携オプション
if(method_exists($LACNE->library["setting"] , 'get_connect_info'))
{
    //接続情報を取得
    $connect_info = $LACNE->library["setting"]->get_connect_info();
    
    //すでにアカウント認証しているかどうか
    if(isset($connect_info["twitter"]["account"]) && $connect_info["twitter"]["account"])
    {
        //認証済みならindexへ
        fn_redirect("twitter_index.php");
    }
    
    //設定画面でTwitterのCONSUMER KEYを登録していれば
    else if(isset($connect_info["twitter"]["key1"]) && $connect_info["twitter"]["key1"] && isset($connect_info["twitter"]["key2"]) && $connect_info["twitter"]["key2"])
    {
        //認証処理へ
        
        //コールバック
        //成功ならアカウント情報をDB保存してindexへ
        if(isset($_GET["callback"]) && $_GET["callback"] == "on")
        {
            /* If the oauth_token is old redirect to the connect page. */
            if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
              $_SESSION['oauth_status'] = 'oldtoken';
              die("ERROR(Callback)");
            }

            /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
            $connection = new TwitterOAuth($connect_info["twitter"]["key1"], $connect_info["twitter"]["key2"], $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

            /* Request access tokens from twitter */
            $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

            /* Save the access tokens. Normally these would be saved in a database for future use. */
            $_SESSION['access_token'] = $access_token;

            /* Remove no longer needed request tokens */
            unset($_SESSION['oauth_token']);
            unset($_SESSION['oauth_token_secret']);

            /* If HTTP response is 200 continue otherwise send to connect page to retry */
            if (200 == $connection->http_code) {
                /* The user has been verified and the access tokens can be saved for future use */
                $_SESSION['status'] = 'verified';
                //var_dump($_SESSION);exit;
                
                if(isset($_SESSION["access_token"]["oauth_token"]))
                {
                    $updata = array(
                        "type" => "twitter",
                        "token1" => $_SESSION["access_token"]["oauth_token"],
                        "token2" => $_SESSION["access_token"]["oauth_token_secret"],
                        "account" => $_SESSION["access_token"]["screen_name"],
                        "modified" => fn_get_date()
                    );
                    
                    with(new SocialConnect())->replace($updata , "type");
        
                }
                
                fn_redirect("twitter_index.php");
                
            } else {
              die("ERROR(HTTP response)");
            }
        }
        else
        {
            //コールバックでないなら認証画面へ遷移する処理を
            //このファイル下部の処理で
        }
    }
    else
    {
        die("Twitterのアプリケーション登録をおこなって頂き、CONSUMER_KEYとCONSUMER_SECRETを登録して下さい。");
    }
}
else
{
    exit;
}


/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth($connect_info["twitter"]["key1"], $connect_info["twitter"]["key2"]);
 
/* Get temporary credentials. */
$callback_url = LACNE::get_self_url()."?callback=on";
$request_token = $connection->getRequestToken($callback_url);

/* Save temporary credentials to session. */
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
 
/* If last connection failed don't display authorization link. */
switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token);
    header('Location: ' . $url); 
    break;
  default:
    /* Show notification if something went wrong. */
    echo 'Could not connect to Twitter. Refresh the page or try again later.';
}

?>
<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $('a[href=#box-forget]').click(function(e){
            $('#box-forget iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/forget.php?type=iframe');
            e.preventDefault();
    });
    $('#Main').css('height','auto');
    MainHeight();
    $(window).resize(function(){
            $('#Main').css('height','auto');
            MainHeight();
    });
    
});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    $.library.css3('#Main .login-box,#Main .login-box .head-line03','<?=LACNE_SHAREDATA_PATH?>/');
	
    
    $("#btn_login").click(function(){
        
        var login_account = $("input[name^=login_account]").val();
        var password = $("input[name^=password]").val();
        
        var loading_pos_h = '20%';
        if(device == "smp") loading_pos_h = '30%'; 
        
        $.ajax({
            type:'POST',
            url : '<?=LACNE_APP_ADMIN_PATH?>/login.php',
            data : 'login_account='+login_account+"&password="+password+"&login_ajax=1",
            dataType : 'json',
            success : function(data)
            {
                if(data.status == "1" && data.path)
                {
                    $("#Main").fadeOut(400);
                    $("#Content")
                    .append('<div style="width:100%;margin-top:'+loading_pos_h+';margin-bottom:100px"><p style="width:24px;margin:0 auto"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ajax-loader.gif" width="24" /></p></div>')
                    .delay(700).queue(function() {
                        window.location.href = data.path;
                    });;
                }
                else
                {
                    $("#error_message").html(data.error);
                    $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
                }
            }
        });
        
        return false;
    });
    
    //Output Error モーダルで表示
    <?php
    if(isset($err) && $err) {
    ?>
            $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
    <?php
    }
    ?>


});

function MainHeight(){
    var countentHeight = $('#Main').height()+$('#GlobalHeader').outerHeight({margin: true})+$('#GlobalFooter').outerHeight({margin: true});
    if($(window).height() > countentHeight){
        $('#Content').height($(window).height()-$('#GlobalHeader').outerHeight({margin: true})-$('#GlobalFooter').outerHeight({margin: true}));
        $('#Main').css({'margin-top':'-'+($('#Main').height())/2+'px'});
    }else{
        $('#Content').height(countentHeight);
    }
}


// -----------------------------------------------
</script>

export default {
  srcDir: '.',
  distDir: 'dist',
  baseDir: '',
  gzip: false,
  webp: false,
  proxy: 'http://smile-recruit.localhost/'
}
